# car_server
#### 源码不含html和sql，若需要完整源码，需付费，可提供各种定制化服务
#### 源码不含html和sql，若需要完整源码，需付费，可提供各种定制化服务
#### 源码不含html和sql，若需要完整源码，需付费，可提供各种定制化服务
#### 介绍
汽车售后服务系统
SpringBoot,Spring,Mybatis,Thymeleaf 

汽车服务系统是一款同时面向C端和B端的一个综合服务平台，主要包含售后管理、订单管理、财务管理、考核管理、库存管理、会员管理等功能；

主要的技术为：服务端（后端）： 整体框架spring-boot，权限框架shiro，数据库连接方式jdbc，数据层框架mybatis，分页工具pagehelper，json解析器fastjson

前端：模板引擎thymeleaf，前端框架bootstrap，页面采用html+css+jss模式


![输入图片说明](wps1.jpg)
![输入图片说明](wps2.jpg)
![输入图片说明](wps3.jpg)
![输入图片说明](wps4.jpg)
![输入图片说明](wps5.jpg)
![输入图片说明](wps6.jpg)
![输入图片说明](wps7.jpg)
![输入图片说明](wps8.jpg)
![输入图片说明](wps9.jpg)
![输入图片说明](wps10.jpg)
![输入图片说明](wps11.jpg)
![输入图片说明](wps12.jpg)
![输入图片说明](wps13.jpg)
![输入图片说明](wps14.jpg)
![输入图片说明](wps15.jpg)
![输入图片说明](wps16.jpg)
![输入图片说明](wps17.jpg)
![输入图片说明](wps18.jpg)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230101114729.jpg)



### **登录**


#### **登录验证**

用户输入账号密码，后端校验账号密码是否批准，决定是否允许登录

@PostMapping("/login")

@ResponseBody

public AjaxResult ajaxLogin(String username, String password, Boolean rememberMe)

{

  UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);

  Subject subject = SecurityUtils.**getSubject**();

  try

  {

​    subject.login(token);

​    return success();

  }

  catch (AuthenticationException e)

  {

​    String msg = "用户或密码错误";

​    if (StringUtils.**isNotEmpty**(e.getMessage()))

​    {

​      msg = e.getMessage();

​    }

​    return error(msg);

  }

}

 

 

 

### **注册**

#### **注册****操作**

顾客输入注册信息，点击注册按钮后，调用注册接口，进行账号的非重判断之后，进行用户的新增操作

**/****

 ***** **注册**

 ***/**

public String register(SysUser user)

{

  String msg = "", loginName = user.getLoginName(), password = user.getPassword();

 

  if (ShiroConstants.**CAPTCHA_ERROR**.equals(ServletUtils.**getRequest**().getAttribute(ShiroConstants.**CURRENT_CAPTCHA**)))

  {

​    msg = "验证码错误";

  }

  else if (StringUtils.**isEmpty**(loginName))

  {

​    msg = "用户名不能为空";

  }

  else if (StringUtils.**isEmpty**(password))

  {

​    msg = "用户密码不能为空";

  }

  else if (password.length() < UserConstants.**PASSWORD_MIN_LENGTH**

​      || password.length() > UserConstants.**PASSWORD_MAX_LENGTH**)

  {

​    msg = "密码长度必须在5到20个字符之间";

  }

  else if (loginName.length() < UserConstants.**USERNAME_MIN_LENGTH**

​      || loginName.length() > UserConstants.**USERNAME_MAX_LENGTH**)

  {

​    msg = "账户长度必须在2到20个字符之间";

  }

  else if (UserConstants.**USER_NAME_NOT_UNIQUE**.equals(userService.checkLoginNameUnique(loginName)))

  {

​    msg = "保存用户'" + loginName + "'失败，注册账号已存在";

  }

  else

  {

​    user.setPwdUpdateDate(DateUtils.**getNowDate**());

​    user.setUserName(loginName);

​    user.setSalt(ShiroUtils.**randomSalt**());

​    user.setPassword(passwordService.encryptPassword(user.getLoginName(), user.getPassword(), user.getSalt()));

​    boolean regFlag = userService.registerUser(user);

​    if (!regFlag)

​    {

​      msg = "注册失败,请联系系统管理人员";

​    }

​    else

​    {

​      AsyncManager.**me**().execute(AsyncFactory.**recordLogininfor**(loginName, Constants.**REGISTER**, MessageUtils.**message**("user.register.success")));

​    }

  }

  return msg;

}

 

 

### **顾客首页**

对于顾客，首页展示售后的预约记录和展示设置的宣传轮播图

#### **预约记录查询**

**/****

 ***** **查询售后列表**

*****

 ***** ***\**@param\**\*** **carAfter** **售后**

 ***** ***\**@return\**\*** **售后**

 ***/**

@Override

public List<CarAfter> selectCarAfterList(CarAfter carAfter) {

  return carAfterMapper.selectCarAfterList(carAfter);

}

 

#### **轮播图查询**

**/****

 ***** **查询轮播图列表**

*****

 ***** ***\**@param\**\*** **carBanner** **轮播图**

 ***** ***\**@return\**\*** **轮播图**

 ***/**

@Override

public List<CarBanner> selectCarBannerList(CarBanner carBanner)

{

  return carBannerMapper.selectCarBannerList(carBanner);

}

 

 

 

### **管理员首页**


对于管理员，首页主要是营业情况的数据，包含当日和当月的营业数据，以及当年各月的营业数据；今年的营业情况中，新增订单的范围是创建时间在该月的订单，收款订单的范围是收款时间在该月的订单；此外，下方会展示所有顾客的预约进度列表

 
 

#### **预约记录查询**

**/****

 ***** **查询售后列表**

*****

 ***** ***\**@param\**\*** **carAfter** **售后**

 ***** ***\**@return\**\*** **售后**

 ***/**

@Override

public List<CarAfter> selectCarAfterList(CarAfter carAfter) {

  return carAfterMapper.selectCarAfterList(carAfter);

}

 

 

### **个人信息**


 

此处功能为个人的信息展示，以及修改基本资料或者修改密码的操作；

 

#### **修改资料接口**

**/****

 ***** **修改用户**

 ***/**

@Log(title = "个人信息", businessType = BusinessType.**UPDATE**)

@PostMapping("/update")

@ResponseBody

public AjaxResult update(CarSysUser user)

{

 

  SysUser currentUser = getSysUser();

  user.setUserId(getUserId());

  currentUser.setUserName(user.getUserName());

  currentUser.setEmail(user.getEmail());

  currentUser.setPhonenumber(user.getPhonenumber());

  currentUser.setSex(user.getSex());

  if (StringUtils.**isNotEmpty**(user.getPhonenumber())

​      && UserConstants.**USER_PHONE_NOT_UNIQUE**.equals(userService.checkPhoneUnique(currentUser)))

  {

​    return error("修改用户'" + currentUser.getLoginName() + "'失败，手机号码已存在");

  }

  else if (StringUtils.**isNotEmpty**(user.getEmail())

​      && UserConstants.**USER_EMAIL_NOT_UNIQUE**.equals(userService.checkEmailUnique(currentUser)))

  {

​    return error("修改用户'" + currentUser.getLoginName() + "'失败，邮箱账号已存在");

  }

  if (userService.updateUserInfo(currentUser) > 0)

  {

​    carSysUserService.updateCarSysUser(user);

​    setSysUser(userService.selectUserById(currentUser.getUserId()));

​    return success();

  }

  return error();

}

 

#### **修改密码接口**

@Log(title = "重置密码", businessType = BusinessType.**UPDATE**)

@PostMapping("/resetPwd")

@ResponseBody

public AjaxResult resetPwd(String oldPassword, String newPassword)

{

  SysUser user = getSysUser();

  if (!passwordService.matches(user, oldPassword))

  {

​    return error("修改密码失败，旧密码错误");

  }

  if (passwordService.matches(user, newPassword))

  {

​    return error("新密码不能与旧密码相同");

  }

  user.setSalt(ShiroUtils.**randomSalt**());

  user.setPassword(passwordService.encryptPassword(user.getLoginName(), newPassword, user.getSalt()));

  user.setPwdUpdateDate(DateUtils.**getNowDate**());

  if (userService.resetUserPwd(user) > 0)

  {

​    setSysUser(userService.selectUserById(user.getUserId()));

​    return success();

  }

  return error("修改密码异常，请联系管理员");

}

 

 

### **轮播图管理**


 此模块用来定义顾客端首页的宣传轮播图，此处进行管理；

 

轮播图管理相关的接口为：

src/main/java/com/ruoyi/web/controller/car/CarBannerController.java

 

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，上传图片点保存后，调用新增接口保存

**/****

 ***** **新增轮播图**

*****

 ***** ***\**@param\**\*** **carBanner** **轮播图**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertCarBanner(CarBanner carBanner)

{

  carBanner.setCreateTime(DateUtils.**getNowDate**());

  return carBannerMapper.insertCarBanner(carBanner);

}

#### **编辑操作**

点击编辑按钮，打开编辑页面。编辑填完信息，上传图片保存后，调用修改接口保存

**/****

 ***** **修改轮播图**

*****

 ***** ***\**@param\**\*** **carBanner** **轮播图**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateCarBanner(CarBanner carBanner)

{

  carBanner.setUpdateTime(DateUtils.**getNowDate**());

  return carBannerMapper.updateCarBanner(carBanner);

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **删除轮播图信息**

*****

 ***** ***\**@param\**\*** **id** **轮播图主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deleteCarBannerById(Long id)

{

  return carBannerMapper.deleteCarBannerById(id);

}

 

 

 

### **订单管理**

这里记录着营业的一切订单，是营业数据的基础，这里对于顾客而言，是顾客本人订单的查询和查看，对于管理员而言，可以进行订单的新增、编辑、收款、申请退款等操作

 

售后管理相关的接口为：

src/main/java/com/ruoyi/web/controller/car/CarAfterController.java

 

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存，同时扣减商品库存

**/****

 ***** **新增订单**

*****

 ***** ***\**@param\**\*** **carOrder** **订单**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Transactional

@Override

public int insertCarOrder(CarOrder carOrder) {

  carOrder.setCreateTime(DateUtils.**getNowDate**());

  carOrder.setId(SnowflakeIdWorker.**generateId**());

  carOrder.setOrderName(getOrderName(carOrder.getCarOrderItemList()));

 

 

  int rows = carOrderMapper.insertCarOrder(carOrder);

  insertCarOrderItem(carOrder);

  return rows;

}

 

**/****

 ***** **新增清单明细信息**

 ***** **同时扣减库存**

*****

 ***** ***\**@param\**\*** **carOrder** **订单对象**

 ***/**

public void insertCarOrderItem(CarOrder carOrder) {

  List<CarOrderItem> carOrderItemList = carOrder.getCarOrderItemList();

  Long id = carOrder.getId();

  if (StringUtils.**isNotNull**(carOrderItemList)) {

​    List<CarOrderItem> list = new ArrayList<CarOrderItem>();

​    for (CarOrderItem carOrderItem : carOrderItemList) {

​      carOrderItem.setOrderId(id);

​      list.add(carOrderItem);

​    }

​    if (list.size() > 0) {

​      carOrderMapper.batchCarOrderItem(list);

​    }

  }

}

#### **编辑操作**

点击编辑按钮，打开编辑页面。编辑填完信息或者修改售后的状态，保存后，调用修改接口保存；同时进行商品的库存变更操作

**/****

 ***** **修改订单**

*****

 ***** ***\**@param\**\*** **carOrder** **订单**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Transactional

@Override

public int updateCarOrder(CarOrder carOrder, boolean justUpdate) {

  carOrder.setUpdateTime(DateUtils.**getNowDate**());

  if (!justUpdate) {

​    carOrderMapper.deleteCarOrderItemByOrderId(carOrder.getId());

​    carOrder.setOrderName(getOrderName(carOrder.getCarOrderItemList()));

​    insertCarOrderItem(carOrder);

  }

  return carOrderMapper.updateCarOrder(carOrder);

}

 

 

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录，同时归还商品库存

**/****

 ***** **删除订单信息**

*****

 ***** ***\**@param\**\*** **id** **订单主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Transactional

@Override

public int deleteCarOrderById(Long id) {

  carOrderMapper.deleteCarOrderItemByOrderId(id);

  return carOrderMapper.deleteCarOrderById(id);

}

 

#### **收款****操作**

点击收款按钮，会二次弹窗确认，点击确认后，会调用收款按钮，同时对顾客的余额进行判断，没问题后，正常扣费，并将订单收款状态置为已收款。同时生成财务账单记录

**/****

 ***** **订单收款**

 ***/**

@Log(title = "订单收款", businessType = BusinessType.**UPDATE**)

@PostMapping("/pay")

@ResponseBody

public AjaxResult pay(Long id) {

 

  CarOrder carOrderExist = carOrderService.selectCarOrderById(id);

  CarSysUser carSysUser = carSysUserService.selectCarSysUserByUserId(carOrderExist.getUserId());

  BigDecimal balance = carSysUser.getBalance() == null ? BigDecimal.**ZERO** : carSysUser.getBalance();

  BigDecimal salesPrice = carOrderExist.getSalesPrice() == null ? BigDecimal.**ZERO** : carOrderExist.getSalesPrice();

 

  if (salesPrice.compareTo(balance) == 1) {

​    return error("客户余额不足！请充值！");

  }

 

  //修改客户余额

  carSysUser.setBalance(balance.subtract(salesPrice));

  carSysUserService.updateCarSysUser(carSysUser);

 

 

  CarOrder carOrder = new CarOrder();

  carOrder.setId(id);

  carOrder.setIsPay("Y");

  carOrder.setPaymentTime(DateUtils.**getNowDate**());

  //同时生成财务记录

  carFinanceService.insertCarFinance(id, true);

  return toAjax(carOrderService.updateCarOrder(carOrder, true));

}

 

#### **申请****退款****操作**

点击申请退款按钮后，将订单状态改为退款中，等待后续的同意退款

**/****

 ***** **订单申请退款**

 ***/**

@Log(title = "订单申请退款", businessType = BusinessType.**UPDATE**)

@PostMapping("/backMoney")

@ResponseBody

public AjaxResult backMoney(Long id) {

  CarOrder carOrder = new CarOrder();

  carOrder.setId(id);

  carOrder.setStatus("2");

 

  return toAjax(carOrderService.updateCarOrder(carOrder, true));

}

 

 

#### **同意退款操作**

点击同意退款后，将款项退回顾客的余额，归还商品库存，同时生成财务的记录；

**/****

 ***** **订单同意退款**

 ***/**

@Log(title = "订单同意退款", businessType = BusinessType.**UPDATE**)

@PostMapping("/auditBack")

@ResponseBody

public AjaxResult auditBack(Long id) {

  CarOrder carOrder = new CarOrder();

  carOrder.setId(id);

  carOrder.setStatus("3");

  //同时生成财务记录

  carFinanceService.insertCarFinance(id, false);

 

  //已存在的商品明细,还原库存

  CarOrderItem query = new CarOrderItem();

  query.setOrderId(Long.**valueOf**(id));

  List<CarOrderItem> carOrderItems = carOrderItemService.selectCarOrderItemList(query);

  carGoodsService.deductionGoods(null, carOrderItems);

 

  return toAjax(carOrderService.updateCarOrder(carOrder, true));

}

 

 

 

 

 

### **售后管理**

 

此模块用来进行售后的一些预约等级之类的操作，包括保养，维修，召回等功能；

 

售后管理相关的接口为：

src/main/java/com/ruoyi/web/controller/car/CarAfterController.java

 

#### **新增****操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增售后**

*****

 ***** ***\**@param\**\*** **carAfter** **售后**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertCarAfter(CarAfter carAfter)

{

  carAfter.setCreateTime(DateUtils.**getNowDate**());

  return carAfterMapper.insertCarAfter(carAfter);

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息或者修改售后的状态，保存后，调用修改接口保存

**/****

 ***** **修改售后**

*****

 ***** ***\**@param\**\*** **carAfter** **售后**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateCarAfter(CarAfter carAfter)

{

  carAfter.setUpdateTime(DateUtils.**getNowDate**());

  return carAfterMapper.updateCarAfter(carAfter);

}

#### **删除****操作**

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **删除售后信息**

*****

 ***** ***\**@param\**\*** **id** **售后主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deleteCarAfterById(Long id)

{

  return carAfterMapper.deleteCarAfterById(id);

}

 

 

 

 

### **财务管理**


此模块用来对订单进行自动记账，或者管理在此处进行财务账单的新增操作等，订单生成的账单无法编辑，手动录入的账单可以编辑；

 

财务管理相关的接口为：

src/main/java/com/ruoyi/web/controller/car/CarFinanceController.java

 

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增财务明细**

*****

 ***** ***\**@param\**\*** **carFinance** **财务明细**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertCarFinance(CarFinance carFinance) {

  carFinance.setCreateTime(DateUtils.**getNowDate**());

  return carFinanceMapper.insertCarFinance(carFinance);

}

 

 

#### **编辑操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改财务明细**

*****

 ***** ***\**@param\**\*** **carFinance** **财务明细**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateCarFinance(CarFinance carFinance) {

  carFinance.setUpdateTime(DateUtils.**getNowDate**());

  return carFinanceMapper.updateCarFinance(carFinance);

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **删除财务明细信息**

*****

 ***** ***\**@param\**\*** **id** **财务明细主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deleteCarFinanceById(Long id) {

  return carFinanceMapper.deleteCarFinanceById(id);

}

 

 

 

### **库存管理**


此模块用来对库存进行管理，用于订单新增时的商品选择，以及库存的相关查询

 

库存管理相关的接口为：

src/main/java/com/ruoyi/web/controller/car/CarGoodsController.java

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增库存**

*****

 ***** ***\**@param\**\*** **carGoods** **库存**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertCarGoods(CarGoods carGoods) {

  carGoods.setCreateTime(DateUtils.**getNowDate**());

  return carGoodsMapper.insertCarGoods(carGoods);

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改库存**

*****

 ***** ***\**@param\**\*** **carGoods** **库存**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateCarGoods(CarGoods carGoods) {

  carGoods.setUpdateTime(DateUtils.**getNowDate**());

  return carGoodsMapper.updateCarGoods(carGoods);

}

#### **删除****操作**

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **删除库存信息**

*****

 ***** ***\**@param\**\*** **id** **库存主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deleteCarGoodsById(Long id) {

  return carGoodsMapper.deleteCarGoodsById(id);

}

 

#### **库存变更的逻辑核心代码**

**/****

 ***** **修改变更库存**

*****

 ***** ***\**@param\**\*** **deductList** **库存扣减**

 ***** ***\**@param\**\*** **addList**   **库存新增**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public boolean deductionGoods(List<CarOrderItem> deductList, List<CarOrderItem> addList) {

  Map<Long, Integer> deductData = null == deductList ? new HashMap<>() : deductList.stream().collect(Collectors.**toMap**(CarOrderItem::getGoodId, CarOrderItem::getGoodAmount));

  Map<Long, Integer> addData = null == addList ? new HashMap<>() : addList.stream().collect(Collectors.**toMap**(CarOrderItem::getGoodId, CarOrderItem::getGoodAmount));

 

  //需扣减的集合

  Set<Long> goodIds = null == deductList ? new HashSet<>() : deductList.stream().map(CarOrderItem::getGoodId).collect(Collectors.**toSet**());

  //需增加的集合

  goodIds.addAll(null == addList ? new HashSet<>() : addList.stream().map(CarOrderItem::getGoodId).collect(Collectors.**toSet**()));

 

  List<CarGoods> goodsList = new ArrayList<>();

  for (Long goodId : goodIds) {

​    CarGoods carGoods = carGoodsMapper.selectCarGoodsById(goodId);

​    Integer goodAmount = carGoods.getGoodAmount();

​    Integer deduct = deductData.getOrDefault(goodId, 0);

​    Integer add = addData.getOrDefault(goodId, 0);

​    //若库存不够扣减，需要给出提示

​    if (goodAmount + add < deduct) {

​      return false;

​    }

​    //修改库存

​    carGoods.setGoodAmount(goodAmount + add - deduct);

​    goodsList.add(carGoods);

  }

 

  //批量修改库存

  for (CarGoods carGoods : goodsList) {

​    carGoodsMapper.updateCarGoods(carGoods);

  }

 

 

  return true;

}

 

 

### **职员管理**


此模块用来对职员的信息进行管理，包括部门，岗位等基本信息的设置等操作；

 

职员管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysUserController.java

 

 

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存用户信息**

*****

 ***** ***\**@param\**\*** **user** **用户信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int insertUser(SysUser user) {

  // 新增用户信息

  int rows = userMapper.insertUser(user);

  // 新增用户岗位关联

  insertUserPost(user);

  // 新增用户与角色管理

  insertUserRole(user.getUserId(), user.getRoleIds());

  return rows;

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存用户信息**

*****

 ***** ***\**@param\**\*** **user** **用户信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int updateUser(SysUser user) {

  Long userId = user.getUserId();

  // 删除用户与角色关联

  userRoleMapper.deleteUserRoleByUserId(userId);

  // 新增用户与角色管理

  insertUserRole(user.getUserId(), user.getRoleIds());

  // 删除用户与岗位关联

  userPostMapper.deleteUserPostByUserId(userId);

  // 新增用户与岗位管理

  insertUserPost(user);

  return userMapper.updateUser(user);

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **批量删除用户信息**

*****

 ***** ***\**@param\**\*** **ids** **需要删除的数据****ID**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int deleteUserByIds(String ids) {

  Long[] userIds = Convert.**toLongArray**(ids);

  for (Long userId : userIds) {

​    checkUserAllowed(new SysUser(userId));

​    checkUserDataScope(userId);

  }

  // 删除用户与角色关联

  userRoleMapper.deleteUserRole(userIds);

  // 删除用户与岗位关联

  userPostMapper.deleteUserPost(userIds);

  return userMapper.deleteUserByIds(userIds);

}

 

 

 

 

### **绩效考核**


此模块由管理员维护，为了新增和记录职员的考核数据，职员每月仅允许产生一条考核数据

 

绩效考核相关的接口为：

 

src/main/java/com/ruoyi/web/controller/car/CarCheckMonthController.java

#### **新增****操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存,需要对重复性进行校验

**/****

 ***** **新增考核**

*****

 ***** ***\**@param\**\*** **carCheckMonth** **考核**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertCarCheckMonth(CarCheckMonth carCheckMonth)

{

CarCheckMonth carCheckMonthExist = carCheckMonthMapper.selectCarCheckMonthOne(carCheckMonth);

  if (null != carCheckMonthExist) {

  //return error("该职员该月考核已存在！");

   }

 

  carCheckMonth.setCreateTime(DateUtils.**getNowDate**());

  return carCheckMonthMapper.insertCarCheckMonth(carCheckMonth);

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存,需要对重复性进行校验

**/****

 ***** **修改考核**

*****

 ***** ***\**@param\**\*** **carCheckMonth** **考核**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateCarCheckMonth(CarCheckMonth carCheckMonth)

{

  carCheckMonth.setUpdateTime(DateUtils.**getNowDate**());

  return carCheckMonthMapper.updateCarCheckMonth(carCheckMonth);

}

#### **删除操作**

 

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **删除考核信息**

*****

 ***** ***\**@param\**\*** **id** **考核主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deleteCarCheckMonthById(Long id)

{

  return carCheckMonthMapper.deleteCarCheckMonthById(id);

}

 

 

 

### **会员管理**


此模块用来管理顾客信息，查询顾客的余额等信息，可以进行新增、编辑和充值等操作。

 

会员管理相关的接口为：

src/main/java/com/ruoyi/web/controller/car/CarSysUserController.java

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存,同时自动创建顾客的登录信息

**/****

 ***** **新增保存用户信息**

 ***/**

@RequiresPermissions("car:user:add")

@Log(title = "用户信息", businessType = BusinessType.**INSERT**)

@PostMapping("/add")

@ResponseBody

public AjaxResult addSave(CarSysUser carSysUser) {

  carSysUser.setRoleIds(new Long[]{2L});

  carSysUser.setPostIds(new Long[]{});

  carSysUser.setSalt(ShiroUtils.**randomSalt**());

  carSysUser.setPassword(passwordService.encryptPassword(carSysUser.getLoginName(), carSysUser.getPassword(), carSysUser.getSalt()));

  carSysUser.setCreateBy(getLoginName());

  sysUserService.insertUser(carSysUser);

  return toAjax(carSysUserService.insertCarSysUser(carSysUser));

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改用户信息**

*****

 ***** ***\**@param\**\*** **carSysUser** **用户信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateCarSysUser(CarSysUser carSysUser)

{

  return carSysUserMapper.updateCarSysUser(carSysUser);

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录

**/****

 ***** **删除用户信息信息**

*****

 ***** ***\**@param\**\*** **userId** **用户信息主键**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deleteCarSysUserByUserId(Long userId)

{

  return carSysUserMapper.deleteCarSysUserByUserId(userId);

}

 

 

 

 

### **角色管理**

角色管理是权限管理里重要的一环，设置角色的菜单权限范围，然后通过给账号赋予不同的角色来控制不同账号具有不一样的菜单权限

 

角色管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysRoleController.java

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存角色信息**

*****

 ***** ***\**@param\**\*** **role** **角色信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int insertRole(SysRole role)

{

  // 新增角色信息

  roleMapper.insertRole(role);

  return insertRoleMenu(role);

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存角色信息**

*****

 ***** ***\**@param\**\*** **role** **角色信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int updateRole(SysRole role)

{

  // 修改角色信息

  roleMapper.updateRole(role);

  // 删除角色与菜单关联

  roleMenuMapper.deleteRoleMenuByRoleId(role.getRoleId());

  return insertRoleMenu(role);

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录。需要判断角色没有被其他用户绑定

**/****

 ***** **批量删除角色信息**

*****

 ***** ***\**@param\**\*** **ids** **需要删除的数据****ID**

 ***** ***\**@throws\**\*** **Exception**

 ***/**

@Override

@Transactional

public int deleteRoleByIds(String ids)

{

  Long[] roleIds = Convert.**toLongArray**(ids);

  for (Long roleId : roleIds)

  {

​    checkRoleAllowed(new SysRole(roleId));

​    checkRoleDataScope(roleId);

​    SysRole role = selectRoleById(roleId);

​    if (countUserRoleByRoleId(roleId) > 0)

​    {

​      throw new ServiceException(String.**format**("%1$s已分配,不能删除", role.getRoleName()));

​    }

  }

  // 删除角色与菜单关联

  roleMenuMapper.deleteRoleMenu(roleIds);

  // 删除角色与部门关联

  roleDeptMapper.deleteRoleDept(roleIds);

  return roleMapper.deleteRoleByIds(roleIds);

}

 

 

 

 

 

### **菜单管理**


左侧操作菜单的数据来源，在这里进行相关的配置操作

 

菜单管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysMenuController.java

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存菜单**

 ***/**

@Log(title = "菜单管理", businessType = BusinessType.**INSERT**)

@RequiresPermissions("system:menu:add")

@PostMapping("/add")

@ResponseBody

public AjaxResult addSave(@Validated SysMenu menu)

{

  if (UserConstants.**MENU_NAME_NOT_UNIQUE**.equals(menuService.checkMenuNameUnique(menu)))

  {

​    return error("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");

  }

  menu.setCreateBy(getLoginName());

  AuthorizationUtils.**clearAllCachedAuthorizationInfo**();

  return toAjax(menuService.insertMenu(menu));

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存菜单**

 ***/**

@Log(title = "菜单管理", businessType = BusinessType.**UPDATE**)

@RequiresPermissions("system:menu:edit")

@PostMapping("/edit")

@ResponseBody

public AjaxResult editSave(@Validated SysMenu menu)

{

  if (UserConstants.**MENU_NAME_NOT_UNIQUE**.equals(menuService.checkMenuNameUnique(menu)))

  {

​    return error("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");

  }

  menu.setUpdateBy(getLoginName());

  AuthorizationUtils.**clearAllCachedAuthorizationInfo**();

  return toAjax(menuService.updateMenu(menu));

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录。

**/****

 ***** **删除菜单**

 ***/**

@Log(title = "菜单管理", businessType = BusinessType.**DELETE**)

@RequiresPermissions("system:menu:remove")

@GetMapping("/remove/{menuId}")

@ResponseBody

public AjaxResult remove(@PathVariable("menuId") Long menuId)

{

  if (menuService.selectCountMenuByParentId(menuId) > 0)

  {

​    return AjaxResult.**warn**("存在子菜单,不允许删除");

  }

  if (menuService.selectCountRoleMenuByMenuId(menuId) > 0)

  {

​    return AjaxResult.**warn**("菜单已分配,不允许删除");

  }

  AuthorizationUtils.**clearAllCachedAuthorizationInfo**();

  return toAjax(menuService.deleteMenuById(menuId));

}

 

 

 

### **部门管理**


部门是职工的基本信息中的一项

 

部门管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysDeptController.java

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存部门信息**

*****

 ***** ***\**@param\**\*** **dept** **部门信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertDept(SysDept dept)

{

  SysDept info = deptMapper.selectDeptById(dept.getParentId());

  // 如果父节点不为"正常"状态,则不允许新增子节点

  if (!UserConstants.**DEPT_NORMAL**.equals(info.getStatus()))

  {

​    throw new ServiceException("部门停用，不允许新增");

  }

  dept.setAncestors(info.getAncestors() + "," + dept.getParentId());

  return deptMapper.insertDept(dept);

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存部门信息**

*****

 ***** ***\**@param\**\*** **dept** **部门信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int updateDept(SysDept dept)

{

  SysDept newParentDept = deptMapper.selectDeptById(dept.getParentId());

  SysDept oldDept = selectDeptById(dept.getDeptId());

  if (StringUtils.**isNotNull**(newParentDept) && StringUtils.**isNotNull**(oldDept))

  {

​    String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();

​    String oldAncestors = oldDept.getAncestors();

​    dept.setAncestors(newAncestors);

​    updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);

  }

  int result = deptMapper.updateDept(dept);

  if (UserConstants.**DEPT_NORMAL**.equals(dept.getStatus()) && StringUtils.**isNotEmpty**(dept.getAncestors())

​      && !StringUtils.**equals**("0", dept.getAncestors()))

  {

​    // 如果该部门是启用状态，则启用该部门的所有上级部门

​    updateParentDeptStatusNormal(dept);

  }

  return result;

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录。删除部门前需要判断是否满足删除条件

**/****

 ***** **删除**

 ***/**

@Log(title = "部门管理", businessType = BusinessType.**DELETE**)

@RequiresPermissions("system:dept:remove")

@GetMapping("/remove/{deptId}")

@ResponseBody

public AjaxResult remove(@PathVariable("deptId") Long deptId)

{

  if (deptService.selectDeptCount(deptId) > 0)

  {

​    return AjaxResult.**warn**("存在下级部门,不允许删除");

  }

  if (deptService.checkDeptExistUser(deptId))

  {

​    return AjaxResult.**warn**("部门存在用户,不允许删除");

  }

  deptService.checkDeptDataScope(deptId);

  return toAjax(deptService.deleteDeptById(deptId));

}

 

### **岗位管理**


岗位是职工的基本信息中的一项

 

 

岗位管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysPostController.java

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存岗位**

 ***/**

@RequiresPermissions("system:post:add")

@Log(title = "岗位管理", businessType = BusinessType.**INSERT**)

@PostMapping("/add")

@ResponseBody

public AjaxResult addSave(@Validated SysPost post)

{

  if (UserConstants.**POST_NAME_NOT_UNIQUE**.equals(postService.checkPostNameUnique(post)))

  {

​    return error("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");

  }

  else if (UserConstants.**POST_CODE_NOT_UNIQUE**.equals(postService.checkPostCodeUnique(post)))

  {

​    return error("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");

  }

  post.setCreateBy(getLoginName());

  return toAjax(postService.insertPost(post));

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存岗位**

 ***/**

@RequiresPermissions("system:post:edit")

@Log(title = "岗位管理", businessType = BusinessType.**UPDATE**)

@PostMapping("/edit")

@ResponseBody

public AjaxResult editSave(@Validated SysPost post)

{

  if (UserConstants.**POST_NAME_NOT_UNIQUE**.equals(postService.checkPostNameUnique(post)))

  {

​    return error("修改岗位'" + post.getPostName() + "'失败，岗位名称已存在");

  }

  else if (UserConstants.**POST_CODE_NOT_UNIQUE**.equals(postService.checkPostCodeUnique(post)))

  {

​    return error("修改岗位'" + post.getPostName() + "'失败，岗位编码已存在");

  }

  post.setUpdateBy(getLoginName());

  return toAjax(postService.updatePost(post));

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录。

**/****

 ***** **批量删除岗位信息**

*****

 ***** ***\**@param\**\*** **ids** **需要删除的数据****ID**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int deletePostByIds(String ids)

{

  Long[] postIds = Convert.**toLongArray**(ids);

  for (Long postId : postIds)

  {

​    SysPost post = selectPostById(postId);

​    if (countUserPostById(postId) > 0)

​    {

​      throw new ServiceException(String.**format**("%1$s已分配,不能删除", post.getPostName()));

​    }

  }

  return postMapper.deletePostByIds(postIds);

}

 

### **字典管理**


字段管理的作用是系统中的一些选项的定义，用于下拉选择框和单选框等页面提供数据的支持

 

字典管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysDictTypeController.java

 

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存字典类型信息**

*****

 ***** ***\**@param\**\*** **dict** **字典类型信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertDictType(SysDictType dict)

{

  int row = dictTypeMapper.insertDictType(dict);

  if (row > 0)

  {

​    DictUtils.**setDictCache**(dict.getDictType(), null);

  }

  return row;

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存字典类型信息**

*****

 ***** ***\**@param\**\*** **dict** **字典类型信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

@Transactional

public int updateDictType(SysDictType dict)

{

  SysDictType oldDict = dictTypeMapper.selectDictTypeById(dict.getDictId());

  dictDataMapper.updateDictDataType(oldDict.getDictType(), dict.getDictType());

  int row = dictTypeMapper.updateDictType(dict);

  if (row > 0)

  {

​    List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(dict.getDictType());

​    DictUtils.**setDictCache**(dict.getDictType(), dictDatas);

  }

  return row;

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录。

**/****

 ***** **批量删除字典类型**

*****

 ***** ***\**@param\**\*** **ids** **需要删除的数据**

 ***/**

@Override

public void deleteDictTypeByIds(String ids)

{

  Long[] dictIds = Convert.**toLongArray**(ids);

  for (Long dictId : dictIds)

  {

​    SysDictType dictType = selectDictTypeById(dictId);

​    if (dictDataMapper.countDictDataByType(dictType.getDictType()) > 0)

​    {

​      throw new ServiceException(String.**format**("%1$s已分配,不能删除", dictType.getDictName()));

​    }

​    dictTypeMapper.deleteDictTypeById(dictId);

​    DictUtils.**removeDictCache**(dictType.getDictType());

  }

}

 

 

### **字典数据管理**

字段数据管理是字典管理的扩展，用来定义字典类型下面具有哪些类型，用于下拉选择框和单选框等页面提供数据的支持

 

字典管理相关的接口为：

src/main/java/com/ruoyi/web/controller/system/SysDictDataController.java

 

#### **新增操作**

点击新增按钮，打开新增页面。新增填完信息，点保存后，调用新增接口保存

**/****

 ***** **新增保存字典数据信息**

*****

 ***** ***\**@param\**\*** **data** **字典数据信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int insertDictData(SysDictData data)

{

  int row = dictDataMapper.insertDictData(data);

  if (row > 0)

  {

​    List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());

​    DictUtils.**setDictCache**(data.getDictType(), dictDatas);

  }

  return row;

}

#### **编辑****操作**

点击编辑按钮，打开编辑页面。编辑填完信息，保存后，调用修改接口保存

**/****

 ***** **修改保存字典数据信息**

*****

 ***** ***\**@param\**\*** **data** **字典数据信息**

 ***** ***\**@return\**\*** **结果**

 ***/**

@Override

public int updateDictData(SysDictData data)

{

  int row = dictDataMapper.updateDictData(data);

  if (row > 0)

  {

​    List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());

​    DictUtils.**setDictCache**(data.getDictType(), dictDatas);

  }

  return row;

}

#### **删除操作**

点击删除按钮，调用删除按钮，删除此条记录。

**/****

 ***** **批量删除字典数据**

*****

 ***** ***\**@param\**\*** **ids** **需要删除的数据**

 ***/**

@Override

public void deleteDictDataByIds(String ids)

{

  Long[] dictCodes = Convert.**toLongArray**(ids);

  for (Long dictCode : dictCodes)

  {

​    SysDictData data = selectDictDataById(dictCode);

​    dictDataMapper.deleteDictDataById(dictCode);

​    List<SysDictData> dictDatas = dictDataMapper.selectDictDataByType(data.getDictType());

​    DictUtils.**setDictCache**(data.getDictType(), dictDatas);

  }

}



