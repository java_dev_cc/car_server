package com.ruoyi.web.controller.car;

import java.util.List;

import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarCheckMonth;
import com.ruoyi.car.service.ICarCheckMonthService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 考核Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/check")
public class CarCheckMonthController extends BaseController {
    private String prefix = "car/check";

    @Autowired
    private ICarCheckMonthService carCheckMonthService;
    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("car:check:view")
    @GetMapping()
    public String check() {
        return prefix + "/check";
    }

    /**
     * 查询考核列表
     */
    @RequiresPermissions("car:check:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarCheckMonth carCheckMonth) {
        startPage();
        List<CarCheckMonth> list = carCheckMonthService.selectCarCheckMonthList(carCheckMonth);
        return getDataTable(list);
    }

    /**
     * 导出考核列表
     */
    @RequiresPermissions("car:check:export")
    @Log(title = "考核", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarCheckMonth carCheckMonth) {
        List<CarCheckMonth> list = carCheckMonthService.selectCarCheckMonthList(carCheckMonth);
        ExcelUtil<CarCheckMonth> util = new ExcelUtil<CarCheckMonth>(CarCheckMonth.class);
        return util.exportExcel(list, "考核数据");
    }

    /**
     * 查看考核详情
     */
    @RequiresPermissions("car:check:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap) {
        CarCheckMonth carCheckMonth = carCheckMonthService.selectCarCheckMonthById(id);
        mmap.put("carCheckMonth", carCheckMonth);
        return prefix + "/detail";
    }

    /**
     * 新增考核
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("userList", userService.selectUserListRange(getSysUser(), "00"));
        return prefix + "/add";
    }

    /**
     * 新增保存考核
     */
    @RequiresPermissions("car:check:add")
    @Log(title = "考核", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarCheckMonth carCheckMonth) {


        CarCheckMonth carCheckMonthExist = carCheckMonthService.selectCarCheckMonthOne(carCheckMonth);
        if (null != carCheckMonthExist) {
            return error("该职员该月考核已存在！");
        }

        return toAjax(carCheckMonthService.insertCarCheckMonth(carCheckMonth));
    }

    /**
     * 修改考核
     */
    @RequiresPermissions("car:check:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        CarCheckMonth carCheckMonth = carCheckMonthService.selectCarCheckMonthById(id);
        mmap.put("carCheckMonth", carCheckMonth);
        mmap.put("userList", userService.selectUserListRange(getSysUser(), "00"));
        return prefix + "/edit";
    }

    /**
     * 修改保存考核
     */
    @RequiresPermissions("car:check:edit")
    @Log(title = "考核", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarCheckMonth carCheckMonth) {
        CarCheckMonth carCheckMonthExist = carCheckMonthService.selectCarCheckMonthOne(carCheckMonth);
        if (null != carCheckMonthExist) {
            return error("该职员该月考核已存在！");
        }
        return toAjax(carCheckMonthService.updateCarCheckMonth(carCheckMonth));
    }

    /**
     * 删除考核
     */
    @RequiresPermissions("car:check:remove")
    @Log(title = "考核", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(carCheckMonthService.deleteCarCheckMonthByIds(ids));
    }
}
