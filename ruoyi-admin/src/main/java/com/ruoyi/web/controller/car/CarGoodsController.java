package com.ruoyi.web.controller.car;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarGoods;
import com.ruoyi.car.service.ICarGoodsService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库存Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/goods")
public class CarGoodsController extends BaseController
{
    private String prefix = "car/goods";

    @Autowired
    private ICarGoodsService carGoodsService;

    @RequiresPermissions("car:goods:view")
    @GetMapping()
    public String goods()
    {
        return prefix + "/goods";
    }

    /**
     * 查询库存列表
     */
    @RequiresPermissions("car:goods:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarGoods carGoods)
    {
        startPage();
        List<CarGoods> list = carGoodsService.selectCarGoodsList(carGoods);
        return getDataTable(list);
    }

    /**
     * 导出库存列表
     */
    @RequiresPermissions("car:goods:export")
    @Log(title = "库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarGoods carGoods)
    {
        List<CarGoods> list = carGoodsService.selectCarGoodsList(carGoods);
        ExcelUtil<CarGoods> util = new ExcelUtil<CarGoods>(CarGoods.class);
        return util.exportExcel(list, "库存数据");
    }

    /**
     * 查看库存详情
     */
    @RequiresPermissions("car:goods:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarGoods carGoods = carGoodsService.selectCarGoodsById(id);
        mmap.put("carGoods", carGoods);
        return prefix + "/detail";
    }

    /**
     * 新增库存
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存库存
     */
    @RequiresPermissions("car:goods:add")
    @Log(title = "库存", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarGoods carGoods)
    {
        return toAjax(carGoodsService.insertCarGoods(carGoods));
    }

    /**
     * 修改库存
     */
    @RequiresPermissions("car:goods:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarGoods carGoods = carGoodsService.selectCarGoodsById(id);
        mmap.put("carGoods", carGoods);
        return prefix + "/edit";
    }

    /**
     * 修改保存库存
     */
    @RequiresPermissions("car:goods:edit")
    @Log(title = "库存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarGoods carGoods)
    {
        return toAjax(carGoodsService.updateCarGoods(carGoods));
    }

    /**
     * 删除库存
     */
    @RequiresPermissions("car:goods:remove")
    @Log(title = "库存", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carGoodsService.deleteCarGoodsByIds(ids));
    }
}
