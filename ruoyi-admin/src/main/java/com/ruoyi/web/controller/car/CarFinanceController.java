package com.ruoyi.web.controller.car;

import java.util.List;

import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarFinance;
import com.ruoyi.car.service.ICarFinanceService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 财务明细Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/finance")
public class CarFinanceController extends BaseController
{
    private String prefix = "car/finance";
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ICarFinanceService carFinanceService;

    @RequiresPermissions("car:finance:view")
    @GetMapping()
    public String finance()
    {
        return prefix + "/finance";
    }

    /**
     * 查询财务明细列表
     */
    @RequiresPermissions("car:finance:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarFinance carFinance)
    {
        startPage();
        List<CarFinance> list = carFinanceService.selectCarFinanceList(carFinance);
        return getDataTable(list);
    }

    /**
     * 导出财务明细列表
     */
    @RequiresPermissions("car:finance:export")
    @Log(title = "财务明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarFinance carFinance)
    {
        List<CarFinance> list = carFinanceService.selectCarFinanceList(carFinance);
        ExcelUtil<CarFinance> util = new ExcelUtil<CarFinance>(CarFinance.class);
        return util.exportExcel(list, "财务明细数据");
    }

    /**
     * 查看财务明细详情
     */
    @RequiresPermissions("car:finance:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarFinance carFinance = carFinanceService.selectCarFinanceById(id);
        mmap.put("carFinance", carFinance);
        return prefix + "/detail";
    }

    /**
     * 新增财务明细
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("userList", userService.selectUserListRange(getSysUser(),"01"));
        return prefix + "/add";
    }

    /**
     * 新增保存财务明细
     */
    @RequiresPermissions("car:finance:add")
    @Log(title = "财务明细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarFinance carFinance)
    {
        carFinance.setFinaSource("1");
        return toAjax(carFinanceService.insertCarFinance(carFinance));
    }

    /**
     * 修改财务明细
     */
    @RequiresPermissions("car:finance:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarFinance carFinance = carFinanceService.selectCarFinanceById(id);
        mmap.put("carFinance", carFinance);
        mmap.put("userList", userService.selectUserListRange(getSysUser(),"01"));
        return prefix + "/edit";
    }

    /**
     * 修改保存财务明细
     */
    @RequiresPermissions("car:finance:edit")
    @Log(title = "财务明细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarFinance carFinance)
    {
        return toAjax(carFinanceService.updateCarFinance(carFinance));
    }

    /**
     * 删除财务明细
     */
    @RequiresPermissions("car:finance:remove")
    @Log(title = "财务明细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carFinanceService.deleteCarFinanceByIds(ids));
    }
}
