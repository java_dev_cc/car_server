package com.ruoyi.web.controller.car;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarAfter;
import com.ruoyi.car.service.ICarAfterService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 售后Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/after")
public class CarAfterController extends BaseController {
    private String prefix = "car/after";

    @Autowired
    private ICarAfterService carAfterService;
    @Autowired
    private ISysUserService userService;


    @RequiresPermissions("car:after:view")
    @GetMapping()
    public String after() {
        return prefix + "/after";
    }

    /**
     * 查询售后列表
     */
    @RequiresPermissions("car:after:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarAfter carAfter) {
        if (isUser()) {
            carAfter.setUserId(getUserId());
        }
        startPage();
        List<CarAfter> list = carAfterService.selectCarAfterList(carAfter);
        return getDataTable(list);
    }

    /**
     * 导出售后列表
     */
    @RequiresPermissions("car:after:export")
    @Log(title = "售后", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarAfter carAfter) {
        List<CarAfter> list = carAfterService.selectCarAfterList(carAfter);
        ExcelUtil<CarAfter> util = new ExcelUtil<CarAfter>(CarAfter.class);
        return util.exportExcel(list, "售后数据");
    }

    /**
     * 查看售后详情
     */
    @RequiresPermissions("car:after:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap) {
        CarAfter carAfter = carAfterService.selectCarAfterById(id);
        mmap.put("carAfter", carAfter);
        return prefix + "/detail";
    }

    /**
     * 新增售后
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("userList", userService.selectUserListRange(getSysUser(), "01"));
        return prefix + "/add";
    }

    /**
     * 新增保存售后
     */
    @RequiresPermissions("car:after:add")
    @Log(title = "售后", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarAfter carAfter) {
        return toAjax(carAfterService.insertCarAfter(carAfter));
    }

    /**
     * 修改售后
     */
    @RequiresPermissions("car:after:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        CarAfter carAfter = carAfterService.selectCarAfterById(id);
        mmap.put("carAfter", carAfter);
        mmap.put("userList", userService.selectUserListRange(getSysUser(), "01"));
        return prefix + "/edit";
    }

    /**
     * 修改保存售后
     */
    @RequiresPermissions("car:after:edit")
    @Log(title = "售后", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarAfter carAfter) {
        return toAjax(carAfterService.updateCarAfter(carAfter));
    }

    /**
     * 删除售后
     */
    @RequiresPermissions("car:after:remove")
    @Log(title = "售后", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(carAfterService.deleteCarAfterByIds(ids));
    }
}
