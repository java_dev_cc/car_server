package com.ruoyi.web.controller.car;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarBanner;
import com.ruoyi.car.service.ICarBannerService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车辆信息Controller
 *
 * @author cuic
 * @date 2022-05-03
 */
@Controller
@RequestMapping("/car/banner")
public class CarBannerController extends BaseController
{
    private String prefix = "car/banner";

    @Autowired
    private ICarBannerService carBannerService;

    @RequiresPermissions("car:banner:view")
    @GetMapping()
    public String banner()
    {
        return prefix + "/banner";
    }

    /**
     * 查询车辆信息列表
     */
    @RequiresPermissions("car:banner:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarBanner carBanner)
    {
        startPage();
        List<CarBanner> list = carBannerService.selectCarBannerList(carBanner);
        return getDataTable(list);
    }

    /**
     * 导出车辆信息列表
     */
    @RequiresPermissions("car:banner:export")
    @Log(title = "车辆信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarBanner carBanner)
    {
        List<CarBanner> list = carBannerService.selectCarBannerList(carBanner);
        ExcelUtil<CarBanner> util = new ExcelUtil<CarBanner>(CarBanner.class);
        return util.exportExcel(list, "车辆信息数据");
    }

    /**
     * 查看车辆信息详情
     */
    @RequiresPermissions("car:banner:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarBanner carBanner = carBannerService.selectCarBannerById(id);
        mmap.put("carBanner", carBanner);
        return prefix + "/detail";
    }

    /**
     * 新增车辆信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存车辆信息
     */
    @RequiresPermissions("car:banner:add")
    @Log(title = "车辆信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarBanner carBanner)
    {
        return toAjax(carBannerService.insertCarBanner(carBanner));
    }

    /**
     * 修改车辆信息
     */
    @RequiresPermissions("car:banner:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarBanner carBanner = carBannerService.selectCarBannerById(id);
        mmap.put("carBanner", carBanner);
        return prefix + "/edit";
    }

    /**
     * 修改保存车辆信息
     */
    @RequiresPermissions("car:banner:edit")
    @Log(title = "车辆信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarBanner carBanner)
    {
        return toAjax(carBannerService.updateCarBanner(carBanner));
    }

    /**
     * 删除车辆信息
     */
    @RequiresPermissions("car:banner:remove")
    @Log(title = "车辆信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carBannerService.deleteCarBannerByIds(ids));
    }
}
