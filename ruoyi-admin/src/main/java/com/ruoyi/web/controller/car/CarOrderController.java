package com.ruoyi.web.controller.car;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.car.domain.CarOrderItem;
import com.ruoyi.car.domain.CarSysUser;
import com.ruoyi.car.service.*;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.service.ISysUserService;
import io.swagger.models.auth.In;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarOrder;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/order")
public class CarOrderController extends BaseController {
    private String prefix = "car/order";

    @Autowired
    private ICarOrderService carOrderService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private ICarSysUserService carSysUserService;
    @Autowired
    private ICarGoodsService carGoodsService;
    @Autowired
    private ICarOrderItemService carOrderItemService;
    @Autowired
    private ICarFinanceService carFinanceService;

    @RequiresPermissions("car:order:view")
    @GetMapping()
    public String order() {
        return prefix + "/order";
    }

    /**
     * 查询订单列表
     */
    @RequiresPermissions("car:order:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarOrder carOrder) {
        if (isUser()) {
            carOrder.setUserId(getUserId());
        }
        startPage();
        List<CarOrder> list = carOrderService.selectCarOrderList(carOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @RequiresPermissions("car:order:export")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarOrder carOrder) {
        List<CarOrder> list = carOrderService.selectCarOrderList(carOrder);
        ExcelUtil<CarOrder> util = new ExcelUtil<CarOrder>(CarOrder.class);
        return util.exportExcel(list, "订单数据");
    }

    /**
     * 查看订单详情
     */
    @RequiresPermissions("car:order:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap) {
        CarOrder carOrder = carOrderService.selectCarOrderById(id);
        mmap.put("carOrder", carOrder);

        return prefix + "/detail";
    }

    /**
     * 新增订单
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("userList", userService.selectUserListRange(getSysUser(), "01"));
        mmap.put("goodsList", carGoodsService.selectCarGoodsList(null));
        return prefix + "/add";
    }

    /**
     * 新增保存订单
     */
    @RequiresPermissions("car:order:add")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarOrder carOrder) {


        boolean flag = carGoodsService.deductionGoods(carOrder.getCarOrderItemList(), null);
        if (!flag) {
            error("库存不足，请修改后再提交订单！");
        }

        return toAjax(carOrderService.insertCarOrder(carOrder));
    }

    /**
     * 修改订单
     */
    @RequiresPermissions("car:order:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        CarOrder carOrder = carOrderService.selectCarOrderById(id);
        mmap.put("carOrder", carOrder);
        mmap.put("userList", userService.selectUserListRange(getSysUser(), "01"));
        mmap.put("goodsList", carGoodsService.selectCarGoodsList(null));
        return prefix + "/edit";
    }

    /**
     * 修改保存订单
     */
    @RequiresPermissions("car:order:edit")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarOrder carOrder) {

        //已存在的商品明细
        CarOrderItem query = new CarOrderItem();
        query.setOrderId(carOrder.getId());
        List<CarOrderItem> carOrderItems = carOrderItemService.selectCarOrderItemList(query);
        boolean flag = carGoodsService.deductionGoods(carOrder.getCarOrderItemList(), carOrderItems);
        if (!flag) {
            error("库存不足，请修改后再提交订单！");
        }
        return toAjax(carOrderService.updateCarOrder(carOrder, false));
    }

    /**
     * 删除订单
     */
    @RequiresPermissions("car:order:remove")
    @Log(title = "订单", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {


        for (String id : ids.split(",")) {
            //已存在的商品明细
            CarOrderItem query = new CarOrderItem();
            query.setOrderId(Long.valueOf(id));
            List<CarOrderItem> carOrderItems = carOrderItemService.selectCarOrderItemList(query);
            carGoodsService.deductionGoods(null, carOrderItems);
        }


        return toAjax(carOrderService.deleteCarOrderByIds(ids));
    }

    /**
     * 订单收款
     */
    @Log(title = "订单收款", businessType = BusinessType.UPDATE)
    @PostMapping("/pay")
    @ResponseBody
    public AjaxResult pay(Long id) {

        CarOrder carOrderExist = carOrderService.selectCarOrderById(id);
        CarSysUser carSysUser = carSysUserService.selectCarSysUserByUserId(carOrderExist.getUserId());
        BigDecimal balance = carSysUser.getBalance() == null ? BigDecimal.ZERO : carSysUser.getBalance();
        BigDecimal salesPrice = carOrderExist.getSalesPrice() == null ? BigDecimal.ZERO : carOrderExist.getSalesPrice();

        if (salesPrice.compareTo(balance) == 1) {
            return error("客户余额不足！请充值！");
        }

        //修改客户余额
        carSysUser.setBalance(balance.subtract(salesPrice));
        carSysUserService.updateCarSysUser(carSysUser);


        CarOrder carOrder = new CarOrder();
        carOrder.setId(id);
        carOrder.setIsPay("Y");
        carOrder.setPaymentTime(DateUtils.getNowDate());
        //同时生成财务记录
        carFinanceService.insertCarFinance(id, true);
        return toAjax(carOrderService.updateCarOrder(carOrder, true));
    }

    /**
     * 订单申请退款
     */
    @Log(title = "订单申请退款", businessType = BusinessType.UPDATE)
    @PostMapping("/backMoney")
    @ResponseBody
    public AjaxResult backMoney(Long id) {
        CarOrder carOrder = new CarOrder();
        carOrder.setId(id);
        carOrder.setStatus("2");

        return toAjax(carOrderService.updateCarOrder(carOrder, true));
    }


    /**
     * 月度统计报表
     */
    @Log(title = "月度统计报表")
    @GetMapping("/map")
    @ResponseBody
    public AjaxResult map(@RequestParam(required = false, defaultValue = "2022") String year) {
        Integer[] create = carOrderService.mapCreate(year);
        Integer[] pay = carOrderService.mapPay(year);
        Integer[][] data = {create, pay};
        return success(data);
    }


    /**
     * 订单同意退款
     */
    @Log(title = "订单同意退款", businessType = BusinessType.UPDATE)
    @PostMapping("/auditBack")
    @ResponseBody
    public AjaxResult auditBack(Long id) {
        CarOrder carOrder = new CarOrder();
        carOrder.setId(id);
        carOrder.setStatus("3");
        //同时生成财务记录
        carFinanceService.insertCarFinance(id, false);

        //已存在的商品明细,还原库存
        CarOrderItem query = new CarOrderItem();
        query.setOrderId(Long.valueOf(id));
        List<CarOrderItem> carOrderItems = carOrderItemService.selectCarOrderItemList(query);
        carGoodsService.deductionGoods(null, carOrderItems);

        return toAjax(carOrderService.updateCarOrder(carOrder, true));
    }
}
