package com.ruoyi.web.controller.car;

import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import com.ruoyi.framework.shiro.service.SysPasswordService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarSysUser;
import com.ruoyi.car.service.ICarSysUserService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户信息Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/user")
public class CarSysUserController extends BaseController {
    private String prefix = "car/user";

    @Autowired
    private ICarSysUserService carSysUserService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private SysPasswordService passwordService;
    @RequiresPermissions("car:user:view")
    @GetMapping()
    public String user() {
        return prefix + "/user";
    }

    /**
     * 查询用户信息列表
     */
    @RequiresPermissions("car:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarSysUser carSysUser) {
        startPage();
        List<CarSysUser> list = carSysUserService.selectCarSysUserList(carSysUser);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @RequiresPermissions("car:user:export")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarSysUser carSysUser) {
        List<CarSysUser> list = carSysUserService.selectCarSysUserList(carSysUser);
        ExcelUtil<CarSysUser> util = new ExcelUtil<CarSysUser>(CarSysUser.class);
        return util.exportExcel(list, "用户信息数据");
    }

    /**
     * 查看用户信息详情
     */
    @RequiresPermissions("car:user:detail")
    @GetMapping("/detail/{userId}")
    public String detail(@PathVariable("userId") Long userId, ModelMap mmap) {
        CarSysUser carSysUser = carSysUserService.selectCarSysUserByUserId(userId);
        mmap.put("carSysUser", carSysUser);
        return prefix + "/detail";
    }

    /**
     * 新增用户信息
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存用户信息
     */
    @RequiresPermissions("car:user:add")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarSysUser carSysUser) {
        carSysUser.setRoleIds(new Long[]{2L});
        carSysUser.setPostIds(new Long[]{});
        carSysUser.setSalt(ShiroUtils.randomSalt());
        carSysUser.setPassword(passwordService.encryptPassword(carSysUser.getLoginName(), carSysUser.getPassword(), carSysUser.getSalt()));
        carSysUser.setCreateBy(getLoginName());
        sysUserService.insertUser(carSysUser);
        return toAjax(carSysUserService.insertCarSysUser(carSysUser));
    }

    /**
     * 修改用户信息
     */
    @RequiresPermissions("car:user:edit")
    @GetMapping("/edit/{userId}")
    public String edit(@PathVariable("userId") Long userId, ModelMap mmap) {
        CarSysUser carSysUser = carSysUserService.selectCarSysUserByUserId(userId);
        mmap.put("carSysUser", carSysUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户信息
     */
    @RequiresPermissions("car:user:edit")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarSysUser carSysUser) {
        carSysUser.setRoleIds(new Long[]{2L});
        carSysUser.setPostIds(new Long[]{});
        sysUserService.updateUser(carSysUser);
        return toAjax(carSysUserService.updateCarSysUser(carSysUser));
    }

    /**
     * 删除用户信息
     */
    @RequiresPermissions("car:user:remove")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(carSysUserService.deleteCarSysUserByUserIds(ids));
    }


    /**
     * 充值
     */
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping("/pay")
    @ResponseBody
    public AjaxResult pay(Long userId, BigDecimal money) {
        CarSysUser carSysUser = carSysUserService.selectCarSysUserByUserId(userId);
        BigDecimal balance = carSysUser.getBalance();
        carSysUser.setBalance(balance.add(money));
        return toAjax( carSysUserService.updateCarSysUser(carSysUser));
    }
}
