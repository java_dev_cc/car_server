package com.ruoyi.web.controller.car;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.car.domain.CarOrderItem;
import com.ruoyi.car.service.ICarOrderItemService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 清单明细Controller
 *
 * @author cuic
 * @date 2022-04-29
 */
@Controller
@RequestMapping("/car/item")
public class CarOrderItemController extends BaseController
{
    private String prefix = "car/item";

    @Autowired
    private ICarOrderItemService carOrderItemService;

    @RequiresPermissions("car:item:view")
    @GetMapping()
    public String item()
    {
        return prefix + "/item";
    }

    /**
     * 查询清单明细列表
     */
    @RequiresPermissions("car:item:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarOrderItem carOrderItem)
    {
        startPage();
        List<CarOrderItem> list = carOrderItemService.selectCarOrderItemList(carOrderItem);
        return getDataTable(list);
    }

    /**
     * 导出清单明细列表
     */
    @RequiresPermissions("car:item:export")
    @Log(title = "清单明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarOrderItem carOrderItem)
    {
        List<CarOrderItem> list = carOrderItemService.selectCarOrderItemList(carOrderItem);
        ExcelUtil<CarOrderItem> util = new ExcelUtil<CarOrderItem>(CarOrderItem.class);
        return util.exportExcel(list, "清单明细数据");
    }

    /**
     * 查看清单明细详情
     */
    @RequiresPermissions("car:item:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarOrderItem carOrderItem = carOrderItemService.selectCarOrderItemById(id);
        mmap.put("carOrderItem", carOrderItem);
        return prefix + "/detail";
    }

    /**
     * 新增清单明细
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存清单明细
     */
    @RequiresPermissions("car:item:add")
    @Log(title = "清单明细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarOrderItem carOrderItem)
    {
        return toAjax(carOrderItemService.insertCarOrderItem(carOrderItem));
    }

    /**
     * 修改清单明细
     */
    @RequiresPermissions("car:item:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CarOrderItem carOrderItem = carOrderItemService.selectCarOrderItemById(id);
        mmap.put("carOrderItem", carOrderItem);
        return prefix + "/edit";
    }

    /**
     * 修改保存清单明细
     */
    @RequiresPermissions("car:item:edit")
    @Log(title = "清单明细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarOrderItem carOrderItem)
    {
        return toAjax(carOrderItemService.updateCarOrderItem(carOrderItem));
    }

    /**
     * 删除清单明细
     */
    @RequiresPermissions("car:item:remove")
    @Log(title = "清单明细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carOrderItemService.deleteCarOrderItemByIds(ids));
    }
}
