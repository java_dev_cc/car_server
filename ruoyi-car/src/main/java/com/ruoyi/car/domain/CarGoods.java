package com.ruoyi.car.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库存对象 car_goods
 * 
 * @author cuic
 * @date 2022-04-29
 */
public class CarGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String goodName;

    /** 单位 */
    @Excel(name = "单位")
    private String goodUnit;

    /** 规格型号 */
    @Excel(name = "规格型号")
    private String goodNorms;

    /** 数量 */
    @Excel(name = "数量")
    private Integer goodAmount;

    /** 颜色 */
    @Excel(name = "颜色")
    private String goodColor;

    /** 类别 */
    @Excel(name = "类别")
    private String goodType;

    /** 厂家 */
    @Excel(name = "厂家")
    private String goodMade;

    /** 成本 */
    @Excel(name = "成本")
    private BigDecimal goodCost;

    /** 售价 */
    @Excel(name = "售价")
    private BigDecimal goodPrice;

    /** 是否删除 */
    private Integer isDel;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setGoodUnit(String goodUnit) 
    {
        this.goodUnit = goodUnit;
    }

    public String getGoodUnit() 
    {
        return goodUnit;
    }
    public void setGoodNorms(String goodNorms) 
    {
        this.goodNorms = goodNorms;
    }

    public String getGoodNorms() 
    {
        return goodNorms;
    }
    public void setGoodAmount(Integer goodAmount) 
    {
        this.goodAmount = goodAmount;
    }

    public Integer getGoodAmount() 
    {
        return goodAmount;
    }
    public void setGoodColor(String goodColor) 
    {
        this.goodColor = goodColor;
    }

    public String getGoodColor() 
    {
        return goodColor;
    }
    public void setGoodType(String goodType) 
    {
        this.goodType = goodType;
    }

    public String getGoodType() 
    {
        return goodType;
    }
    public void setGoodMade(String goodMade) 
    {
        this.goodMade = goodMade;
    }

    public String getGoodMade() 
    {
        return goodMade;
    }
    public void setGoodCost(BigDecimal goodCost) 
    {
        this.goodCost = goodCost;
    }

    public BigDecimal getGoodCost() 
    {
        return goodCost;
    }
    public void setGoodPrice(BigDecimal goodPrice) 
    {
        this.goodPrice = goodPrice;
    }

    public BigDecimal getGoodPrice() 
    {
        return goodPrice;
    }
    public void setIsDel(Integer isDel) 
    {
        this.isDel = isDel;
    }

    public Integer getIsDel() 
    {
        return isDel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodName", getGoodName())
            .append("goodUnit", getGoodUnit())
            .append("goodNorms", getGoodNorms())
            .append("goodAmount", getGoodAmount())
            .append("goodColor", getGoodColor())
            .append("goodType", getGoodType())
            .append("goodMade", getGoodMade())
            .append("goodCost", getGoodCost())
            .append("goodPrice", getGoodPrice())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("isDel", getIsDel())
            .toString();
    }
}
