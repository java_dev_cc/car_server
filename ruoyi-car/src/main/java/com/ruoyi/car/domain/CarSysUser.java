package com.ruoyi.car.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.entity.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户信息对象 car_sys_user
 *
 * @author cuic
 * @date 2022-04-29
 */
public class CarSysUser extends SysUser {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 余额
     */
    @Excel(name = "余额")
    private BigDecimal balance;

    /**
     * 性别
     */
    @Excel(name = "性别")
    private String sex;

    /**
     * 身份证号
     */
    private String cardId;

    /**
     * 出生日期
     */
    private Date birth;

    /**
     * 民族
     */
    @Excel(name = "民族")
    private String nation;

    /**
     * 入职日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入职日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date workdate;

    /**
     * 最高学位
     */
    @Excel(name = "最高学位")
    private String highestdegree;

    /**
     * 政治面貌
     */
    @Excel(name = "政治面貌")
    private String politic;



    /**
     * 工作状态（在岗/离职/退休）
     */
    @Excel(name = "工作状态", readConverterExp = "在=岗/离职/退休")
    private String workstate;



    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public Date getBirth() {
        return birth;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getNation() {
        return nation;
    }

    public void setWorkdate(Date workdate) {
        this.workdate = workdate;
    }

    public Date getWorkdate() {
        return workdate;
    }

    public void setHighestdegree(String highestdegree) {
        this.highestdegree = highestdegree;
    }

    public String getHighestdegree() {
        return highestdegree;
    }

    public void setPolitic(String politic) {
        this.politic = politic;
    }

    public String getPolitic() {
        return politic;
    }

    public void setWorkstate(String workstate) {
        this.workstate = workstate;
    }

    public String getWorkstate() {
        return workstate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("userId", getUserId())
                .append("balance", getBalance())
                .append("sex", getSex())
                .append("cardId", getCardId())
                .append("birth", getBirth())
                .append("nation", getNation())
                .append("workdate", getWorkdate())
                .append("highestdegree", getHighestdegree())
                .append("politic", getPolitic())
                .append("email", getEmail())
                .append("workstate", getWorkstate())
                .toString();
    }
}
