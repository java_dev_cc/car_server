package com.ruoyi.car.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 考核对象 car_check_month
 *
 * @author cuic
 * @date 2022-04-29
 */
public class CarCheckMonth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 员工id */
    @Excel(name = "员工id")
    private String userId;

    /** 考核月 */
    @Excel(name = "考核月")
    private String yearMon;

    /** 考核状态 */
    private String status;

    /** 考核分 */
    @Excel(name = "考核分")
    private String checkScore;
    /** 客户名称 */
    @Excel(name = "客户名称")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setYearMon(String yearMon)
    {
        this.yearMon = yearMon;
    }

    public String getYearMon()
    {
        return yearMon;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setCheckScore(String checkScore)
    {
        this.checkScore = checkScore;
    }

    public String getCheckScore()
    {
        return checkScore;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("yearMon", getYearMon())
            .append("status", getStatus())
            .append("checkScore", getCheckScore())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
