package com.ruoyi.car.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 car_order
 *
 * @author cuic
 * @date 2022-04-29
 */
public class CarOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long userId;

    /**
     * 是否支付
     */
    @Excel(name = "是否支付")
    private String isPay;

    /**
     * 订单状态
     */
    @Excel(name = "订单状态")
    private String status;

    /**
     * 名称
     */
    @Excel(name = "名称")
    private String orderName;

    /**
     * 销售金额
     */
    @Excel(name = "销售金额")
    private BigDecimal salesPrice;

    /**
     * 付款时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentTime;

    /**
     * 是否删除
     */
    @Excel(name = "是否删除")
    private Integer isDel;

    /**
     * 客户名称
     */
    @Excel(name = "客户名称")
    private String userName;


    /**
     * 清单明细信息
     */
    private List<CarOrderItem> carOrderItemList;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setSalesPrice(BigDecimal salesPrice) {
        this.salesPrice = salesPrice;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public List<CarOrderItem> getCarOrderItemList() {
        return carOrderItemList;
    }

    public void setCarOrderItemList(List<CarOrderItem> carOrderItemList) {
        this.carOrderItemList = carOrderItemList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("isPay", getIsPay())
                .append("status", getStatus())
                .append("orderName", getOrderName())
                .append("salesPrice", getSalesPrice())
                .append("paymentTime", getPaymentTime())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("isDel", getIsDel())
                .append("carOrderItemList", getCarOrderItemList())
                .toString();
    }
}
