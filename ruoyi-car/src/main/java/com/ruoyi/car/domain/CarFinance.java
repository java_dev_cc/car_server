package com.ruoyi.car.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 财务明细对象 car_finance
 *
 * @author cuic
 * @date 2022-04-29
 */
public class CarFinance extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderId;

    /** 账期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "账期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finaDate;

    /** 来源 */
    @Excel(name = "来源")
    private String finaSource;

    /** 类型 */
    @Excel(name = "类型")
    private String finaType;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal finaPrice;

    /** 名称 */
    @Excel(name = "名称")
    private String finaName;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;
    /** 客户名称 */
    @Excel(name = "客户名称")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public Long getOrderId()
    {
        return orderId;
    }
    public void setFinaDate(Date finaDate)
    {
        this.finaDate = finaDate;
    }

    public Date getFinaDate()
    {
        return finaDate;
    }
    public void setFinaSource(String finaSource)
    {
        this.finaSource = finaSource;
    }

    public String getFinaSource()
    {
        return finaSource;
    }
    public void setFinaType(String finaType)
    {
        this.finaType = finaType;
    }

    public String getFinaType()
    {
        return finaType;
    }
    public void setFinaPrice(BigDecimal finaPrice)
    {
        this.finaPrice = finaPrice;
    }

    public BigDecimal getFinaPrice()
    {
        return finaPrice;
    }
    public void setFinaName(String finaName)
    {
        this.finaName = finaName;
    }

    public String getFinaName()
    {
        return finaName;
    }
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }

    public Long getUserId()
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("finaDate", getFinaDate())
            .append("finaSource", getFinaSource())
            .append("finaType", getFinaType())
            .append("finaPrice", getFinaPrice())
            .append("finaName", getFinaName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("userId", getUserId())
            .toString();
    }
}
