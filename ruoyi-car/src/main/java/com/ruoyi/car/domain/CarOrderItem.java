package com.ruoyi.car.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 清单明细对象 car_order_item
 * 
 * @author cuic
 * @date 2022-04-29
 */
public class CarOrderItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderId;

    /** 商品Id */
    @Excel(name = "商品Id")
    private Long goodId;

    /** 名称 */
    @Excel(name = "名称")
    private String goodName;

    /** 数量 */
    @Excel(name = "数量")
    private Integer goodAmount;

    /** 售价 */
    @Excel(name = "售价")
    private BigDecimal goodPrice;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setGoodId(Long goodId) 
    {
        this.goodId = goodId;
    }

    public Long getGoodId() 
    {
        return goodId;
    }
    public void setGoodName(String goodName) 
    {
        this.goodName = goodName;
    }

    public String getGoodName() 
    {
        return goodName;
    }
    public void setGoodAmount(Integer goodAmount) 
    {
        this.goodAmount = goodAmount;
    }

    public Integer getGoodAmount() 
    {
        return goodAmount;
    }
    public void setGoodPrice(BigDecimal goodPrice) 
    {
        this.goodPrice = goodPrice;
    }

    public BigDecimal getGoodPrice() 
    {
        return goodPrice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("goodId", getGoodId())
            .append("goodName", getGoodName())
            .append("goodAmount", getGoodAmount())
            .append("goodPrice", getGoodPrice())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
