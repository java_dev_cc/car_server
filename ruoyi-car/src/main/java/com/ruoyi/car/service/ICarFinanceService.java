package com.ruoyi.car.service;

import java.util.List;
import com.ruoyi.car.domain.CarFinance;

/**
 * 财务明细Service接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface ICarFinanceService
{
    /**
     * 查询财务明细
     *
     * @param id 财务明细主键
     * @return 财务明细
     */
    public CarFinance selectCarFinanceById(Long id);

    /**
     * 查询财务明细列表
     *
     * @param carFinance 财务明细
     * @return 财务明细集合
     */
    public List<CarFinance> selectCarFinanceList(CarFinance carFinance);

    int insertCarFinance(Long orderId, boolean receive);

    /**
     * 新增财务明细
     *
     * @param carFinance 财务明细
     * @return 结果
     */
    public int insertCarFinance(CarFinance carFinance);

    /**
     * 修改财务明细
     *
     * @param carFinance 财务明细
     * @return 结果
     */
    public int updateCarFinance(CarFinance carFinance);

    /**
     * 批量删除财务明细
     *
     * @param ids 需要删除的财务明细主键集合
     * @return 结果
     */
    public int deleteCarFinanceByIds(String ids);

    /**
     * 删除财务明细信息
     *
     * @param id 财务明细主键
     * @return 结果
     */
    public int deleteCarFinanceById(Long id);
}
