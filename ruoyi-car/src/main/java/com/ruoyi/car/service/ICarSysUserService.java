package com.ruoyi.car.service;

import java.util.List;
import com.ruoyi.car.domain.CarSysUser;

/**
 * 用户信息Service接口
 * 
 * @author cuic
 * @date 2022-04-29
 */
public interface ICarSysUserService 
{
    /**
     * 查询用户信息
     * 
     * @param userId 用户信息主键
     * @return 用户信息
     */
    public CarSysUser selectCarSysUserByUserId(Long userId);

    /**
     * 查询用户信息列表
     * 
     * @param carSysUser 用户信息
     * @return 用户信息集合
     */
    public List<CarSysUser> selectCarSysUserList(CarSysUser carSysUser);

    /**
     * 新增用户信息
     * 
     * @param carSysUser 用户信息
     * @return 结果
     */
    public int insertCarSysUser(CarSysUser carSysUser);

    /**
     * 修改用户信息
     * 
     * @param carSysUser 用户信息
     * @return 结果
     */
    public int updateCarSysUser(CarSysUser carSysUser);

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户信息主键集合
     * @return 结果
     */
    public int deleteCarSysUserByUserIds(String userIds);

    /**
     * 删除用户信息信息
     * 
     * @param userId 用户信息主键
     * @return 结果
     */
    public int deleteCarSysUserByUserId(Long userId);
}
