package com.ruoyi.car.service.impl;

import java.util.List;

import com.ruoyi.car.domain.CarOrder;
import com.ruoyi.car.mapper.CarOrderMapper;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarFinanceMapper;
import com.ruoyi.car.domain.CarFinance;
import com.ruoyi.car.service.ICarFinanceService;
import com.ruoyi.common.core.text.Convert;

/**
 * 财务明细Service业务层处理
 *
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarFinanceServiceImpl implements ICarFinanceService {
    @Autowired
    private CarFinanceMapper carFinanceMapper;
    @Autowired
    private CarOrderMapper carOrderMapper;

    /**
     * 查询财务明细
     *
     * @param id 财务明细主键
     * @return 财务明细
     */
    @Override
    public CarFinance selectCarFinanceById(Long id) {
        return carFinanceMapper.selectCarFinanceById(id);
    }

    /**
     * 查询财务明细列表
     *
     * @param carFinance 财务明细
     * @return 财务明细
     */
    @Override
    public List<CarFinance> selectCarFinanceList(CarFinance carFinance) {
        return carFinanceMapper.selectCarFinanceList(carFinance);
    }

    /**
     * 新增财务明细
     *
     * @param orderId 订单id
     * @return 结果
     */
    @Override
    public int insertCarFinance(Long orderId, boolean receive) {

        CarOrder carOrder = carOrderMapper.selectCarOrderById(orderId);
        if (null != carOrder) {
            CarFinance carFinance = new CarFinance();
            carFinance.setFinaType(receive ? "1" : "2");
            carFinance.setOrderId(orderId);
            carFinance.setFinaSource("0");
            carFinance.setUserId(carOrder.getUserId());
            carFinance.setFinaPrice(carOrder.getSalesPrice());
            carFinance.setFinaName(carOrder.getOrderName());
            carFinance.setFinaDate(DateUtils.getNowDate());
            carFinance.setRemark(receive ? "订单付款" : "订单退款");
            return insertCarFinance(carFinance);
        }


        return 0;
    }

    /**
     * 新增财务明细
     *
     * @param carFinance 财务明细
     * @return 结果
     */
    @Override
    public int insertCarFinance(CarFinance carFinance) {
        carFinance.setCreateTime(DateUtils.getNowDate());
        return carFinanceMapper.insertCarFinance(carFinance);
    }

    /**
     * 修改财务明细
     *
     * @param carFinance 财务明细
     * @return 结果
     */
    @Override
    public int updateCarFinance(CarFinance carFinance) {
        carFinance.setUpdateTime(DateUtils.getNowDate());
        return carFinanceMapper.updateCarFinance(carFinance);
    }

    /**
     * 批量删除财务明细
     *
     * @param ids 需要删除的财务明细主键
     * @return 结果
     */
    @Override
    public int deleteCarFinanceByIds(String ids) {
        return carFinanceMapper.deleteCarFinanceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除财务明细信息
     *
     * @param id 财务明细主键
     * @return 结果
     */
    @Override
    public int deleteCarFinanceById(Long id) {
        return carFinanceMapper.deleteCarFinanceById(id);
    }
}
