package com.ruoyi.car.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.ruoyi.car.domain.CarOrderItem;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarGoodsMapper;
import com.ruoyi.car.domain.CarGoods;
import com.ruoyi.car.service.ICarGoodsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 库存Service业务层处理
 *
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarGoodsServiceImpl implements ICarGoodsService {
    @Autowired
    private CarGoodsMapper carGoodsMapper;

    /**
     * 查询库存
     *
     * @param id 库存主键
     * @return 库存
     */
    @Override
    public CarGoods selectCarGoodsById(Long id) {
        return carGoodsMapper.selectCarGoodsById(id);
    }

    /**
     * 查询库存列表
     *
     * @param carGoods 库存
     * @return 库存
     */
    @Override
    public List<CarGoods> selectCarGoodsList(CarGoods carGoods) {
        return carGoodsMapper.selectCarGoodsList(carGoods);
    }

    /**
     * 新增库存
     *
     * @param carGoods 库存
     * @return 结果
     */
    @Override
    public int insertCarGoods(CarGoods carGoods) {
        carGoods.setCreateTime(DateUtils.getNowDate());
        return carGoodsMapper.insertCarGoods(carGoods);
    }

    /**
     * 修改库存
     *
     * @param carGoods 库存
     * @return 结果
     */
    @Override
    public int updateCarGoods(CarGoods carGoods) {
        carGoods.setUpdateTime(DateUtils.getNowDate());
        return carGoodsMapper.updateCarGoods(carGoods);
    }

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的库存主键
     * @return 结果
     */
    @Override
    public int deleteCarGoodsByIds(String ids) {
        return carGoodsMapper.deleteCarGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除库存信息
     *
     * @param id 库存主键
     * @return 结果
     */
    @Override
    public int deleteCarGoodsById(Long id) {
        return carGoodsMapper.deleteCarGoodsById(id);
    }

    /**
     * 修改变更库存
     *
     * @param deductList 库存扣减
     * @param addList    库存新增
     * @return 结果
     */
    @Override
    public boolean deductionGoods(List<CarOrderItem> deductList, List<CarOrderItem> addList) {
        Map<Long, Integer> deductData = null == deductList ? new HashMap<>() : deductList.stream().collect(Collectors.toMap(CarOrderItem::getGoodId, CarOrderItem::getGoodAmount));
        Map<Long, Integer> addData = null == addList ? new HashMap<>() : addList.stream().collect(Collectors.toMap(CarOrderItem::getGoodId, CarOrderItem::getGoodAmount));

        //需扣减的集合
        Set<Long> goodIds = null == deductList ? new HashSet<>() : deductList.stream().map(CarOrderItem::getGoodId).collect(Collectors.toSet());
        //需增加的集合
        goodIds.addAll(null == addList ? new HashSet<>() : addList.stream().map(CarOrderItem::getGoodId).collect(Collectors.toSet()));

        List<CarGoods> goodsList = new ArrayList<>();
        for (Long goodId : goodIds) {
            CarGoods carGoods = carGoodsMapper.selectCarGoodsById(goodId);
            Integer goodAmount = carGoods.getGoodAmount();
            Integer deduct = deductData.getOrDefault(goodId, 0);
            Integer add = addData.getOrDefault(goodId, 0);
            //若库存不够扣减，需要给出提示
            if (goodAmount + add < deduct) {
                return false;
            }
            //修改库存
            carGoods.setGoodAmount(goodAmount + add - deduct);
            goodsList.add(carGoods);
        }

        //批量修改库存
        for (CarGoods carGoods : goodsList) {
            carGoodsMapper.updateCarGoods(carGoods);
        }


        return true;
    }
}
