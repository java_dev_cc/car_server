package com.ruoyi.car.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarSysUserMapper;
import com.ruoyi.car.domain.CarSysUser;
import com.ruoyi.car.service.ICarSysUserService;
import com.ruoyi.common.core.text.Convert;

/**
 * 用户信息Service业务层处理
 * 
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarSysUserServiceImpl implements ICarSysUserService 
{
    @Autowired
    private CarSysUserMapper carSysUserMapper;

    /**
     * 查询用户信息
     * 
     * @param userId 用户信息主键
     * @return 用户信息
     */
    @Override
    public CarSysUser selectCarSysUserByUserId(Long userId)
    {
        return carSysUserMapper.selectCarSysUserByUserId(userId);
    }

    /**
     * 查询用户信息列表
     * 
     * @param carSysUser 用户信息
     * @return 用户信息
     */
    @Override
    public List<CarSysUser> selectCarSysUserList(CarSysUser carSysUser)
    {
        return carSysUserMapper.selectCarSysUserList(carSysUser);
    }

    /**
     * 新增用户信息
     * 
     * @param carSysUser 用户信息
     * @return 结果
     */
    @Override
    public int insertCarSysUser(CarSysUser carSysUser)
    {
        return carSysUserMapper.insertCarSysUser(carSysUser);
    }

    /**
     * 修改用户信息
     * 
     * @param carSysUser 用户信息
     * @return 结果
     */
    @Override
    public int updateCarSysUser(CarSysUser carSysUser)
    {
        return carSysUserMapper.updateCarSysUser(carSysUser);
    }

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteCarSysUserByUserIds(String userIds)
    {
        return carSysUserMapper.deleteCarSysUserByUserIds(Convert.toStrArray(userIds));
    }

    /**
     * 删除用户信息信息
     * 
     * @param userId 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteCarSysUserByUserId(Long userId)
    {
        return carSysUserMapper.deleteCarSysUserByUserId(userId);
    }
}
