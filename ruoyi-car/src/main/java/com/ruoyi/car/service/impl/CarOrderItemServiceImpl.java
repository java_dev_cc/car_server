package com.ruoyi.car.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarOrderItemMapper;
import com.ruoyi.car.domain.CarOrderItem;
import com.ruoyi.car.service.ICarOrderItemService;
import com.ruoyi.common.core.text.Convert;

/**
 * 清单明细Service业务层处理
 * 
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarOrderItemServiceImpl implements ICarOrderItemService 
{
    @Autowired
    private CarOrderItemMapper carOrderItemMapper;

    /**
     * 查询清单明细
     * 
     * @param id 清单明细主键
     * @return 清单明细
     */
    @Override
    public CarOrderItem selectCarOrderItemById(Long id)
    {
        return carOrderItemMapper.selectCarOrderItemById(id);
    }

    /**
     * 查询清单明细列表
     * 
     * @param carOrderItem 清单明细
     * @return 清单明细
     */
    @Override
    public List<CarOrderItem> selectCarOrderItemList(CarOrderItem carOrderItem)
    {
        return carOrderItemMapper.selectCarOrderItemList(carOrderItem);
    }

    /**
     * 新增清单明细
     * 
     * @param carOrderItem 清单明细
     * @return 结果
     */
    @Override
    public int insertCarOrderItem(CarOrderItem carOrderItem)
    {
        carOrderItem.setCreateTime(DateUtils.getNowDate());
        return carOrderItemMapper.insertCarOrderItem(carOrderItem);
    }

    /**
     * 修改清单明细
     * 
     * @param carOrderItem 清单明细
     * @return 结果
     */
    @Override
    public int updateCarOrderItem(CarOrderItem carOrderItem)
    {
        carOrderItem.setUpdateTime(DateUtils.getNowDate());
        return carOrderItemMapper.updateCarOrderItem(carOrderItem);
    }

    /**
     * 批量删除清单明细
     * 
     * @param ids 需要删除的清单明细主键
     * @return 结果
     */
    @Override
    public int deleteCarOrderItemByIds(String ids)
    {
        return carOrderItemMapper.deleteCarOrderItemByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除清单明细信息
     * 
     * @param id 清单明细主键
     * @return 结果
     */
    @Override
    public int deleteCarOrderItemById(Long id)
    {
        return carOrderItemMapper.deleteCarOrderItemById(id);
    }
}
