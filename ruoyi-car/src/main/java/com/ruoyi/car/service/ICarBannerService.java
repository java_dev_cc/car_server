package com.ruoyi.car.service;

import java.util.List;
import com.ruoyi.car.domain.CarBanner;

/**
 * 车辆信息Service接口
 * 
 * @author cuic
 * @date 2022-05-03
 */
public interface ICarBannerService 
{
    /**
     * 查询车辆信息
     * 
     * @param id 车辆信息主键
     * @return 车辆信息
     */
    public CarBanner selectCarBannerById(Long id);

    /**
     * 查询车辆信息列表
     * 
     * @param carBanner 车辆信息
     * @return 车辆信息集合
     */
    public List<CarBanner> selectCarBannerList(CarBanner carBanner);

    /**
     * 新增车辆信息
     * 
     * @param carBanner 车辆信息
     * @return 结果
     */
    public int insertCarBanner(CarBanner carBanner);

    /**
     * 修改车辆信息
     * 
     * @param carBanner 车辆信息
     * @return 结果
     */
    public int updateCarBanner(CarBanner carBanner);

    /**
     * 批量删除车辆信息
     * 
     * @param ids 需要删除的车辆信息主键集合
     * @return 结果
     */
    public int deleteCarBannerByIds(String ids);

    /**
     * 删除车辆信息信息
     * 
     * @param id 车辆信息主键
     * @return 结果
     */
    public int deleteCarBannerById(Long id);
}
