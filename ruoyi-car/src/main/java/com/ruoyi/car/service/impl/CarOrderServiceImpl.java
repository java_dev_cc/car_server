package com.ruoyi.car.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SnowflakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.car.domain.CarOrderItem;
import com.ruoyi.car.mapper.CarOrderMapper;
import com.ruoyi.car.domain.CarOrder;
import com.ruoyi.car.service.ICarOrderService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.util.CollectionUtils;

/**
 * 订单Service业务层处理
 *
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarOrderServiceImpl implements ICarOrderService {
    @Autowired
    private CarOrderMapper carOrderMapper;

    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    @Override
    public CarOrder selectCarOrderById(Long id) {
        return carOrderMapper.selectCarOrderById(id);
    }

    /**
     * 查询订单列表
     *
     * @param carOrder 订单
     * @return 订单
     */
    @Override
    public List<CarOrder> selectCarOrderList(CarOrder carOrder) {
        return carOrderMapper.selectCarOrderList(carOrder);
    }

    /**
     * 新增订单
     *
     * @param carOrder 订单
     * @return 结果
     */
    @Transactional
    @Override
    public int insertCarOrder(CarOrder carOrder) {
        carOrder.setCreateTime(DateUtils.getNowDate());
        carOrder.setId(SnowflakeIdWorker.generateId());
        carOrder.setOrderName(getOrderName(carOrder.getCarOrderItemList()));


        int rows = carOrderMapper.insertCarOrder(carOrder);
        insertCarOrderItem(carOrder);
        return rows;
    }

    /**
     * 获取订单名称
     *
     * @param carOrderItemList 订单
     * @return 结果
     */
    private String getOrderName(List<CarOrderItem> carOrderItemList) {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        if (!CollectionUtils.isEmpty(carOrderItemList)) {

            for (CarOrderItem carOrderItem : carOrderItemList) {
                String goodName = carOrderItem.getGoodName();
                if (i == 0) {
                    sb.append(goodName.split(" ")[0]);
                    i++;
                } else {
                    sb.append(" | ").append(goodName.split(" ")[0]);
                }
            }
        }
        String orderName = sb.toString();
        return orderName.length() < 60 ? orderName : orderName.substring(0, 59) + "...";
    }

    /**
     * 修改订单
     *
     * @param carOrder 订单
     * @return 结果
     */
    @Transactional
    @Override
    public int updateCarOrder(CarOrder carOrder, boolean justUpdate) {
        carOrder.setUpdateTime(DateUtils.getNowDate());
        if (!justUpdate) {
            carOrderMapper.deleteCarOrderItemByOrderId(carOrder.getId());
            carOrder.setOrderName(getOrderName(carOrder.getCarOrderItemList()));
            insertCarOrderItem(carOrder);
        }
        return carOrderMapper.updateCarOrder(carOrder);
    }


    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCarOrderByIds(String ids) {
        carOrderMapper.deleteCarOrderItemByOrderIds(Convert.toStrArray(ids));
        return carOrderMapper.deleteCarOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单信息
     *
     * @param id 订单主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteCarOrderById(Long id) {
        carOrderMapper.deleteCarOrderItemByOrderId(id);
        return carOrderMapper.deleteCarOrderById(id);
    }

    @Override
    public Integer[] mapCreate(String year) {
        return carOrderMapper.mapCreate(year);
    }

    @Override
    public Integer[] mapPay(String year) {
        return carOrderMapper.mapPay(year);
    }

    @Override
    public Map<String, String> countMap() {
        return carOrderMapper.countMap() ;
    }

    /**
     * 新增清单明细信息
     * 同时扣减库存
     *
     * @param carOrder 订单对象
     */
    public void insertCarOrderItem(CarOrder carOrder) {
        List<CarOrderItem> carOrderItemList = carOrder.getCarOrderItemList();
        Long id = carOrder.getId();
        if (StringUtils.isNotNull(carOrderItemList)) {
            List<CarOrderItem> list = new ArrayList<CarOrderItem>();
            for (CarOrderItem carOrderItem : carOrderItemList) {
                carOrderItem.setOrderId(id);
                list.add(carOrderItem);
            }
            if (list.size() > 0) {
                carOrderMapper.batchCarOrderItem(list);
            }
        }
    }
}
