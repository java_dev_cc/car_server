package com.ruoyi.car.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.car.domain.CarOrder;

/**
 * 订单Service接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface ICarOrderService
{
    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    public CarOrder selectCarOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param carOrder 订单
     * @return 订单集合
     */
    public List<CarOrder> selectCarOrderList(CarOrder carOrder);

    /**
     * 新增订单
     *
     * @param carOrder 订单
     * @return 结果
     */
    public int insertCarOrder(CarOrder carOrder);

    /**
     * 修改订单
     *
     * @param carOrder 订单
     * @return 结果
     */
    public int updateCarOrder(CarOrder carOrder,boolean justUpdate);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单主键集合
     * @return 结果
     */
    public int deleteCarOrderByIds(String ids);

    /**
     * 删除订单信息
     *
     * @param id 订单主键
     * @return 结果
     */
    public int deleteCarOrderById(Long id);

    Integer[] mapCreate(String year);

    Integer[] mapPay(String year);

    Map<String, String> countMap();
}
