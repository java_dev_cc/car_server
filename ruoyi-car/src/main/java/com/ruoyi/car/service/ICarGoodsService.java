package com.ruoyi.car.service;

import java.util.List;
import com.ruoyi.car.domain.CarGoods;
import com.ruoyi.car.domain.CarOrderItem;

/**
 * 库存Service接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface ICarGoodsService
{
    /**
     * 查询库存
     *
     * @param id 库存主键
     * @return 库存
     */
    public CarGoods selectCarGoodsById(Long id);

    /**
     * 查询库存列表
     *
     * @param carGoods 库存
     * @return 库存集合
     */
    public List<CarGoods> selectCarGoodsList(CarGoods carGoods);

    /**
     * 新增库存
     *
     * @param carGoods 库存
     * @return 结果
     */
    public int insertCarGoods(CarGoods carGoods);

    /**
     * 修改库存
     *
     * @param carGoods 库存
     * @return 结果
     */
    public int updateCarGoods(CarGoods carGoods);

    /**
     * 批量删除库存
     *
     * @param ids 需要删除的库存主键集合
     * @return 结果
     */
    public int deleteCarGoodsByIds(String ids);

    /**
     * 删除库存信息
     *
     * @param id 库存主键
     * @return 结果
     */
    public int deleteCarGoodsById(Long id);

    boolean deductionGoods(List<CarOrderItem> deductList, List<CarOrderItem> addList);
}
