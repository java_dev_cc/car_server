package com.ruoyi.car.service;

import java.util.List;
import com.ruoyi.car.domain.CarAfter;

/**
 * 售后Service接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface ICarAfterService
{
    /**
     * 查询售后
     *
     * @param id 售后主键
     * @return 售后
     */
    public CarAfter selectCarAfterById(Long id);

    /**
     * 查询售后列表
     *
     * @param carAfter 售后
     * @return 售后集合
     */
    public List<CarAfter> selectCarAfterList(CarAfter carAfter);

    /**
     * 新增售后
     *
     * @param carAfter 售后
     * @return 结果
     */
    public int insertCarAfter(CarAfter carAfter);

    /**
     * 修改售后
     *
     * @param carAfter 售后
     * @return 结果
     */
    public int updateCarAfter(CarAfter carAfter);

    /**
     * 批量删除售后
     *
     * @param ids 需要删除的售后主键集合
     * @return 结果
     */
    public int deleteCarAfterByIds(String ids);

    /**
     * 删除售后信息
     *
     * @param id 售后主键
     * @return 结果
     */
    public int deleteCarAfterById(Long id);

    public List<CarAfter> selectList(CarAfter query);
}
