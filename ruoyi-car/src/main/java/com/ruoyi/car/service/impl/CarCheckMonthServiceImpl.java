package com.ruoyi.car.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarCheckMonthMapper;
import com.ruoyi.car.domain.CarCheckMonth;
import com.ruoyi.car.service.ICarCheckMonthService;
import com.ruoyi.common.core.text.Convert;

/**
 * 考核Service业务层处理
 *
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarCheckMonthServiceImpl implements ICarCheckMonthService
{
    @Autowired
    private CarCheckMonthMapper carCheckMonthMapper;

    /**
     * 查询考核
     *
     * @param id 考核主键
     * @return 考核
     */
    @Override
    public CarCheckMonth selectCarCheckMonthById(Long id)
    {
        return carCheckMonthMapper.selectCarCheckMonthById(id);
    }

    /**
     * 查询考核列表
     *
     * @param carCheckMonth 考核
     * @return 考核
     */
    @Override
    public List<CarCheckMonth> selectCarCheckMonthList(CarCheckMonth carCheckMonth)
    {
        return carCheckMonthMapper.selectCarCheckMonthList(carCheckMonth);
    }

    /**
     * 新增考核
     *
     * @param carCheckMonth 考核
     * @return 结果
     */
    @Override
    public int insertCarCheckMonth(CarCheckMonth carCheckMonth)
    {
        carCheckMonth.setCreateTime(DateUtils.getNowDate());
        return carCheckMonthMapper.insertCarCheckMonth(carCheckMonth);
    }

    /**
     * 修改考核
     *
     * @param carCheckMonth 考核
     * @return 结果
     */
    @Override
    public int updateCarCheckMonth(CarCheckMonth carCheckMonth)
    {
        carCheckMonth.setUpdateTime(DateUtils.getNowDate());
        return carCheckMonthMapper.updateCarCheckMonth(carCheckMonth);
    }

    /**
     * 批量删除考核
     *
     * @param ids 需要删除的考核主键
     * @return 结果
     */
    @Override
    public int deleteCarCheckMonthByIds(String ids)
    {
        return carCheckMonthMapper.deleteCarCheckMonthByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除考核信息
     *
     * @param id 考核主键
     * @return 结果
     */
    @Override
    public int deleteCarCheckMonthById(Long id)
    {
        return carCheckMonthMapper.deleteCarCheckMonthById(id);
    }
    /**
     * 查询考核
     *
     * @param carCheckMonth 考核主键
     * @return 考核
     */
    @Override
    public CarCheckMonth selectCarCheckMonthOne(CarCheckMonth carCheckMonth) {
        return carCheckMonthMapper.selectCarCheckMonthOne(carCheckMonth);
    }
}
