package com.ruoyi.car.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarAfterMapper;
import com.ruoyi.car.domain.CarAfter;
import com.ruoyi.car.service.ICarAfterService;
import com.ruoyi.common.core.text.Convert;

/**
 * 售后Service业务层处理
 *
 * @author cuic
 * @date 2022-04-29
 */
@Service
public class CarAfterServiceImpl implements ICarAfterService {
    @Autowired
    private CarAfterMapper carAfterMapper;

    /**
     * 查询售后
     *
     * @param id 售后主键
     * @return 售后
     */
    @Override
    public CarAfter selectCarAfterById(Long id) {
        return carAfterMapper.selectCarAfterById(id);
    }

    /**
     * 查询售后列表
     *
     * @param carAfter 售后
     * @return 售后
     */
    @Override
    public List<CarAfter> selectCarAfterList(CarAfter carAfter) {
        return carAfterMapper.selectCarAfterList(carAfter);
    }

    /**
     * 新增售后
     *
     * @param carAfter 售后
     * @return 结果
     */
    @Override
    public int insertCarAfter(CarAfter carAfter) {
        carAfter.setCreateTime(DateUtils.getNowDate());
        return carAfterMapper.insertCarAfter(carAfter);
    }

    /**
     * 修改售后
     *
     * @param carAfter 售后
     * @return 结果
     */
    @Override
    public int updateCarAfter(CarAfter carAfter) {
        carAfter.setUpdateTime(DateUtils.getNowDate());
        return carAfterMapper.updateCarAfter(carAfter);
    }

    /**
     * 批量删除售后
     *
     * @param ids 需要删除的售后主键
     * @return 结果
     */
    @Override
    public int deleteCarAfterByIds(String ids) {
        return carAfterMapper.deleteCarAfterByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除售后信息
     *
     * @param id 售后主键
     * @return 结果
     */
    @Override
    public int deleteCarAfterById(Long id) {
        return carAfterMapper.deleteCarAfterById(id);
    }
    /**
     * 查询售后列表
     *
     * @param query 售后
     * @return 售后
     */
    @Override
    public List<CarAfter> selectList(CarAfter query) {
        return carAfterMapper.selectList(query);
    }

}
