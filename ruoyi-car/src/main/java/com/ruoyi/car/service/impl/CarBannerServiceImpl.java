package com.ruoyi.car.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.car.mapper.CarBannerMapper;
import com.ruoyi.car.domain.CarBanner;
import com.ruoyi.car.service.ICarBannerService;
import com.ruoyi.common.core.text.Convert;

/**
 * 轮播图Service业务层处理
 *
 * @author cuic
 * @date 2022-05-03
 */
@Service
public class CarBannerServiceImpl implements ICarBannerService
{
    @Autowired
    private CarBannerMapper carBannerMapper;

    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public CarBanner selectCarBannerById(Long id)
    {
        return carBannerMapper.selectCarBannerById(id);
    }

    /**
     * 查询轮播图列表
     *
     * @param carBanner 轮播图
     * @return 轮播图
     */
    @Override
    public List<CarBanner> selectCarBannerList(CarBanner carBanner)
    {
        return carBannerMapper.selectCarBannerList(carBanner);
    }

    /**
     * 新增轮播图
     *
     * @param carBanner 轮播图
     * @return 结果
     */
    @Override
    public int insertCarBanner(CarBanner carBanner)
    {
        carBanner.setCreateTime(DateUtils.getNowDate());
        return carBannerMapper.insertCarBanner(carBanner);
    }

    /**
     * 修改轮播图
     *
     * @param carBanner 轮播图
     * @return 结果
     */
    @Override
    public int updateCarBanner(CarBanner carBanner)
    {
        carBanner.setUpdateTime(DateUtils.getNowDate());
        return carBannerMapper.updateCarBanner(carBanner);
    }

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteCarBannerByIds(String ids)
    {
        return carBannerMapper.deleteCarBannerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除轮播图信息
     *
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteCarBannerById(Long id)
    {
        return carBannerMapper.deleteCarBannerById(id);
    }
}
