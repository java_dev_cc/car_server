package com.ruoyi.car.mapper;

import java.util.List;
import com.ruoyi.car.domain.CarGoods;

/**
 * 库存Mapper接口
 * 
 * @author cuic
 * @date 2022-04-29
 */
public interface CarGoodsMapper 
{
    /**
     * 查询库存
     * 
     * @param id 库存主键
     * @return 库存
     */
    public CarGoods selectCarGoodsById(Long id);

    /**
     * 查询库存列表
     * 
     * @param carGoods 库存
     * @return 库存集合
     */
    public List<CarGoods> selectCarGoodsList(CarGoods carGoods);

    /**
     * 新增库存
     * 
     * @param carGoods 库存
     * @return 结果
     */
    public int insertCarGoods(CarGoods carGoods);

    /**
     * 修改库存
     * 
     * @param carGoods 库存
     * @return 结果
     */
    public int updateCarGoods(CarGoods carGoods);

    /**
     * 删除库存
     * 
     * @param id 库存主键
     * @return 结果
     */
    public int deleteCarGoodsById(Long id);

    /**
     * 批量删除库存
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarGoodsByIds(String[] ids);
}
