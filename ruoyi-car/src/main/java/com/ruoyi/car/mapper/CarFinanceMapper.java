package com.ruoyi.car.mapper;

import java.util.List;
import com.ruoyi.car.domain.CarFinance;

/**
 * 财务明细Mapper接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface CarFinanceMapper
{
    /**
     * 查询财务明细
     *
     * @param id 财务明细主键
     * @return 清单明细
     */
    public CarFinance selectCarFinanceById(Long id);

    /**
     * 查询清单明细列表
     *
     * @param carFinance 清单明细
     * @return 清单明细集合
     */
    public List<CarFinance> selectCarFinanceList(CarFinance carFinance);

    /**
     * 新增清单明细
     *
     * @param carFinance 清单明细
     * @return 结果
     */
    public int insertCarFinance(CarFinance carFinance);

    /**
     * 修改清单明细
     *
     * @param carFinance 清单明细
     * @return 结果
     */
    public int updateCarFinance(CarFinance carFinance);

    /**
     * 删除清单明细
     *
     * @param id 清单明细主键
     * @return 结果
     */
    public int deleteCarFinanceById(Long id);

    /**
     * 批量删除清单明细
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarFinanceByIds(String[] ids);
}
