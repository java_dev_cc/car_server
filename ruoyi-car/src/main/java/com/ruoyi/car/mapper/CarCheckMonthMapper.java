package com.ruoyi.car.mapper;

import java.util.List;
import com.ruoyi.car.domain.CarCheckMonth;

/**
 * 考核Mapper接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface CarCheckMonthMapper
{
    /**
     * 查询考核
     *
     * @param id 考核主键
     * @return 考核
     */
    public CarCheckMonth selectCarCheckMonthById(Long id);

    /**
     * 查询考核
     *
     * @param carCheckMonth 考核主键
     * @return 考核
     */
    public CarCheckMonth selectCarCheckMonthOne(CarCheckMonth carCheckMonth);
    /**
     * 查询考核列表
     *
     * @param carCheckMonth 考核
     * @return 考核集合
     */
    public List<CarCheckMonth> selectCarCheckMonthList(CarCheckMonth carCheckMonth);

    /**
     * 新增考核
     *
     * @param carCheckMonth 考核
     * @return 结果
     */
    public int insertCarCheckMonth(CarCheckMonth carCheckMonth);

    /**
     * 修改考核
     *
     * @param carCheckMonth 考核
     * @return 结果
     */
    public int updateCarCheckMonth(CarCheckMonth carCheckMonth);

    /**
     * 删除考核
     *
     * @param id 考核主键
     * @return 结果
     */
    public int deleteCarCheckMonthById(Long id);

    /**
     * 批量删除考核
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarCheckMonthByIds(String[] ids);
}
