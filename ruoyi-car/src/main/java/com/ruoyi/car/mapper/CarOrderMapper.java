package com.ruoyi.car.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.car.domain.CarOrder;
import com.ruoyi.car.domain.CarOrderItem;

/**
 * 订单Mapper接口
 *
 * @author cuic
 * @date 2022-04-29
 */
public interface CarOrderMapper
{
    /**
     * 查询订单
     *
     * @param id 订单主键
     * @return 订单
     */
    public CarOrder selectCarOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param carOrder 订单
     * @return 订单集合
     */
    public List<CarOrder> selectCarOrderList(CarOrder carOrder);

    /**
     * 新增订单
     *
     * @param carOrder 订单
     * @return 结果
     */
    public int insertCarOrder(CarOrder carOrder);

    /**
     * 修改订单
     *
     * @param carOrder 订单
     * @return 结果
     */
    public int updateCarOrder(CarOrder carOrder);

    /**
     * 删除订单
     *
     * @param id 订单主键
     * @return 结果
     */
    public int deleteCarOrderById(Long id);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarOrderByIds(String[] ids);

    /**
     * 批量删除清单明细
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarOrderItemByOrderIds(String[] ids);

    /**
     * 批量新增清单明细
     *
     * @param carOrderItemList 清单明细列表
     * @return 结果
     */
    public int batchCarOrderItem(List<CarOrderItem> carOrderItemList);


    /**
     * 通过订单主键删除清单明细信息
     *
     * @param id 订单ID
     * @return 结果
     */
    public int deleteCarOrderItemByOrderId(Long id);

    Integer[] mapCreate(String year);

    Integer[] mapPay(String year);

    Map<String, String> countMap();

}
