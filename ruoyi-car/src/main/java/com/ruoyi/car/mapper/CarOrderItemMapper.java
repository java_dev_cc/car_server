package com.ruoyi.car.mapper;

import java.util.List;
import com.ruoyi.car.domain.CarOrderItem;

/**
 * 清单明细Mapper接口
 * 
 * @author cuic
 * @date 2022-04-29
 */
public interface CarOrderItemMapper 
{
    /**
     * 查询清单明细
     * 
     * @param id 清单明细主键
     * @return 清单明细
     */
    public CarOrderItem selectCarOrderItemById(Long id);

    /**
     * 查询清单明细列表
     * 
     * @param carOrderItem 清单明细
     * @return 清单明细集合
     */
    public List<CarOrderItem> selectCarOrderItemList(CarOrderItem carOrderItem);

    /**
     * 新增清单明细
     * 
     * @param carOrderItem 清单明细
     * @return 结果
     */
    public int insertCarOrderItem(CarOrderItem carOrderItem);

    /**
     * 修改清单明细
     * 
     * @param carOrderItem 清单明细
     * @return 结果
     */
    public int updateCarOrderItem(CarOrderItem carOrderItem);

    /**
     * 删除清单明细
     * 
     * @param id 清单明细主键
     * @return 结果
     */
    public int deleteCarOrderItemById(Long id);

    /**
     * 批量删除清单明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCarOrderItemByIds(String[] ids);
}
